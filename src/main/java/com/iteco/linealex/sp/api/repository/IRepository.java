package com.iteco.linealex.sp.api.repository;

import com.iteco.linealex.sp.exception.InsertExistingEntityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;

public interface IRepository<T> {

    public boolean contains(@NotNull final String entityName);

    public boolean contains(
            @NotNull final String entityName,
            @NotNull final String userId);

    @NotNull
    public Collection<T> findAll();

    @NotNull
    public Collection<T> findAllByName(
            @NotNull final String pattern,
            @NotNull final String userId);

    @Nullable
    public T findOne(@NotNull final String name);

    @Nullable
    public T persist(@NotNull final T example) throws InsertExistingEntityException, NoSuchAlgorithmException;

    @NotNull
    public Collection<T> persist(@NotNull final Collection<T> collection);

    @Nullable
    public T merge(@NotNull final T example) throws InsertExistingEntityException;

    @Nullable
    public T remove(@NotNull final String name);

    @NotNull
    public Collection<T> removeAll();

}