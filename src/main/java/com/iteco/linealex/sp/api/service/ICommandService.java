package com.iteco.linealex.sp.api.service;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ICommandService {

    public void addCommand(@NotNull final AbstractCommand command);

    @Nullable
    public AbstractCommand getCommand(@NotNull final String command);

    @NotNull
    public Collection<AbstractCommand> getCommands();

}