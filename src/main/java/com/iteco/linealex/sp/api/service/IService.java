package com.iteco.linealex.sp.api.service;

import com.iteco.linealex.sp.exception.InsertExistingEntityException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IService<T> {

    @Nullable
    public T selectEntity(
            @Nullable final String entityName,
            @Nullable final String userId);

    @Nullable
    public T persist(@Nullable final T entity) throws InsertExistingEntityException;

    @NotNull
    public Collection<T> persist(@NotNull final Collection<T> collection) throws InsertExistingEntityException;

    @Nullable
    public T merge(@Nullable final T entity) throws InsertExistingEntityException;

    @NotNull
    public Collection<T> getAllEntities();

    @Nullable
    public T removeEntity(@Nullable final String entityName);

    @NotNull
    public Collection<T> removeAllEntities();

}