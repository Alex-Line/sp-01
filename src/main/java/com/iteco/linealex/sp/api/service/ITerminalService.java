package com.iteco.linealex.sp.api.service;

import org.jetbrains.annotations.NotNull;

public interface ITerminalService {

    @NotNull
    public String nextLine();

}