package com.iteco.linealex.sp.command;

import com.iteco.linealex.sp.api.service.ServiceLocator;
import com.iteco.linealex.sp.enumerate.Role;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected ServiceLocator serviceLocator;

    @NotNull
    protected List<Role> roles = Arrays.asList(Role.ADMINISTRATOR, Role.ORDINARY_USER);

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;

    public abstract boolean secure();

    @NotNull
    public List<Role> getAvailableRoles() {
        return roles;
    }

    @NotNull
    public ServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}