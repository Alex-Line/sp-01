package com.iteco.linealex.sp.command.data.load;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.constant.Constants;
import com.iteco.linealex.sp.dto.Domain;
import com.iteco.linealex.sp.exception.TaskManagerException;
import com.iteco.linealex.sp.exception.ThereIsNotSuchFileException;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Collections;

public class DataLoadBinaryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-load-binary";
    }

    @NotNull
    @Override
    public String description() {
        return "LOAD ALL DATA FROM BINARY FILE THE CLASSIC JAVA WAY";
    }

    @Override
    public void execute() throws TaskManagerException, Exception {
        serviceLocator.getSelectedEntityService().clearSelection();
        serviceLocator.getSelectedEntityService().setSelectedUser(null);
        @NotNull final File savedFile = new File(Constants.SAVE_DIR + "data.bin");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final FileInputStream inputStream = new FileInputStream(savedFile);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        if (domain.getUsers() == null) serviceLocator.getUserService().persist(Collections.EMPTY_LIST);
        else serviceLocator.getUserService().persist(domain.getUsers());
        if (domain.getProjects() == null) serviceLocator.getProjectService().persist(Collections.EMPTY_LIST);
        else serviceLocator.getProjectService().persist(domain.getProjects());
        if (domain.getTasks() == null) serviceLocator.getTaskService().persist(Collections.EMPTY_LIST);
        else serviceLocator.getTaskService().persist(domain.getTasks());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}