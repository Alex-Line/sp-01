package com.iteco.linealex.sp.command.project;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.exception.UserIsNotLogInException;
import com.iteco.linealex.sp.util.DateFormatter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "CREATE A NEW PROJECT AND SET SELECTION ON IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        @Nullable final Project project = new Project();
        if (selectedUser == null) throw new UserIsNotLogInException();
        project.setUserId(selectedUser.getId());
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        project.setName(projectName);
        System.out.println("[ENTER PROJECT DESCRIPTION]");
        @NotNull final String projectDescription = serviceLocator.getTerminalService().nextLine();
        project.setDescription(projectDescription);
        System.out.println("[ENTER PROJECT START DATE IN FORMAT: DD.MM.YYYY]");
        project.setDateStart(DateFormatter.formatStringToDate(serviceLocator.getTerminalService().nextLine()));
        System.out.println("[ENTER PROJECT FINISH DATE IN FORMAT: DD.MM.YYYY]");
        project.setDateFinish(DateFormatter.formatStringToDate(serviceLocator.getTerminalService().nextLine()));
        @Nullable final Project insertedEntity = serviceLocator.getProjectService().persist(project);
        if (insertedEntity == null) {
            System.out.println("[THERE IS SOME NULL OR EMPTY DATA]\n");
            return;
        }
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}