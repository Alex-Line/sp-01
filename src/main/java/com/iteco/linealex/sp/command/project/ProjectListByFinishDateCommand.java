package com.iteco.linealex.sp.command.project;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.enumerate.Role;
import com.iteco.linealex.sp.exception.UserIsNotLogInException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class ProjectListByFinishDateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list-finish";
    }

    @NotNull
    @Override
    public String description() {
        return "LIST PROJECTS BY FINISH DATE";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) throw new UserIsNotLogInException();
        System.out.println("[PROJECT LIST]");
        @NotNull Collection<Project> collection = Collections.EMPTY_LIST;
        if (selectedUser.getRole() == Role.ADMINISTRATOR) {
            collection = serviceLocator.getProjectService().getEntitiesSortedByFinishDate();
        } else collection = serviceLocator.getProjectService().getEntitiesSortedByFinishDate(
                selectedUser.getId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY PROJECTS]\n");
            return;
        }
        int index = 1;
        for (Project project : collection) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}