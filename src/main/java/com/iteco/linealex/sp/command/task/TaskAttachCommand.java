package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskAttachCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-attach";
    }

    @NotNull
    @Override
    public String description() {
        return "ATTACH THE SELECTED TASK TO PROJECT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (serviceLocator.getSelectedEntityService().getSelectedTask() == null) {
            System.out.println("[YOU DID NOT SELECT ANY TASK! SELECT ANY AND TRY AGAIN]\n");
            return;
        }
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().getEntityByName(projectName, user.getId());
        boolean result = serviceLocator.getTaskService().attachTaskToProject(project.getId(),
                serviceLocator.getSelectedEntityService().getSelectedTask());
        if (result) System.out.println("[OK]\n");
        else System.out.println("THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}