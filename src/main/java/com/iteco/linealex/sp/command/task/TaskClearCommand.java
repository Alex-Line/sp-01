package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.entity.Task;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.exception.UserIsNotLogInException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVE ALL TASKS FROM THE SELECTED PROJECT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User selectedUser = serviceLocator.getSelectedEntityService().getSelectedUser();
        if (selectedUser == null) throw new UserIsNotLogInException();
        @Nullable final Project selectedProject = serviceLocator.getSelectedEntityService().getSelectedProject();
        @NotNull Collection<Task> collection = Collections.EMPTY_LIST;
        if (selectedProject == null)
            System.out.println("[YOU DID NOT SELECT ANY PROJECT! SELECT ANY AND TRY AGAIN]\n");
        else collection = serviceLocator.getTaskService().removeAllTasksFromProject(
                selectedProject.getId(), selectedUser.getId());
        if (collection.isEmpty()) {
            System.out.println("[YOU DID NOT SELECT ANY PROJECT! SELECT ANY AND TRY AGAIN]\n");
            return;
        }
        System.out.println("[REMOVING TASKS FROM PROJECT]");
        System.out.println("[ALL TASKS REMOVED]");
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}