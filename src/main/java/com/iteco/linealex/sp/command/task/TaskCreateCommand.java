package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.entity.Task;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.util.DateFormatter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "CREATE A NEW TASK IN PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final User user = serviceLocator.getSelectedEntityService().getSelectedUser();
        @Nullable final Project project = serviceLocator.getSelectedEntityService().getSelectedProject();
        @NotNull final Task task = new Task();
        task.setUserId(user.getId());
        System.out.println("[ENTER TASK NAME]");
        @NotNull final String taskName = serviceLocator.getTerminalService().nextLine();
        task.setName(taskName);
        System.out.println("[ENTER TASK DESCRIPTION]");
        @NotNull final String taskDescription = serviceLocator.getTerminalService().nextLine();
        task.setDescription(taskDescription);
        System.out.println("[ENTER TASK START DATE IN FORMAT: DD.MM.YYYY]");
        task.setDateStart(DateFormatter.formatStringToDate(serviceLocator.getTerminalService().nextLine()));
        System.out.println("[ENTER TASK FINISH DATE IN FORMAT: DD.MM.YYYY]");
        task.setDateFinish(DateFormatter.formatStringToDate(serviceLocator.getTerminalService().nextLine()));
        if (project == null) task.setProjectId(null);
        else task.setProjectId(project.getId());
        @Nullable final Task insertedTask = serviceLocator.getTaskService().persist(task);
        if (insertedTask == null) {
            System.out.println("[THERE IS SOME NULL OR EMPTY DATA]\n");
            return;
        }
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}