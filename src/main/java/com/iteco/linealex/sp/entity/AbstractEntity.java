package com.iteco.linealex.sp.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class AbstractEntity implements Serializable {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String name;

}