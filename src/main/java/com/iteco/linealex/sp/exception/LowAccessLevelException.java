package com.iteco.linealex.sp.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class LowAccessLevelException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "YOU DO NOT HAVE RIGHTS TO DO THAT. PLEASE CONTACT ADMINISTRATOR FOR MORE INFORMATION. COMMAND WAS INTERRUPTED\n";
    }

}