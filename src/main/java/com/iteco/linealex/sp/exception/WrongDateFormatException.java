package com.iteco.linealex.sp.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class WrongDateFormatException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "[WRONG DATE FORMAT! COMMAND WAS INTERRUPTED]\n";
    }

}