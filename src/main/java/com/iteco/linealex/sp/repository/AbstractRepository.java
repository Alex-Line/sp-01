package com.iteco.linealex.sp.repository;

import com.iteco.linealex.sp.api.repository.IRepository;
import com.iteco.linealex.sp.entity.AbstractEntity;
import com.iteco.linealex.sp.entity.Task;
import com.iteco.linealex.sp.exception.InsertExistingEntityException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

@NoArgsConstructor
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected final Map<String, T> entityMap = new LinkedHashMap<>();

    public abstract boolean contains(
            @NotNull final String entityName,
            @NotNull final String userId);

    @NotNull
    @Override
    public Collection<T> findAll() {
        @NotNull final Collection<T> collection = new LinkedList<>(entityMap.values());
        return collection;
    }

    @NotNull
    public abstract Collection<T> findAll(@NotNull final String userId);

    @NotNull
    public Collection<T> findAll(
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllByName(@NotNull final String pattern) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    @Override
    public Collection<T> findAllByName(
            @NotNull final String pattern,
            @NotNull final String userId
    ) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllByName(
            @NotNull final String pattern,
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStartDate(
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStartDate(@NotNull final String userId) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStartDate() {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByFinishDate(
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByFinishDate(@NotNull final String userId) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByFinishDate() {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStatus(
            @NotNull final String projectId,
            @NotNull final String userId) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStatus(@NotNull final String userId) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<T> findAllSortedByStatus() {
        return Collections.EMPTY_LIST;
    }

    @Nullable
    @Override
    public T findOne(@NotNull final String entityName) {
        for (@NotNull final Map.Entry<String, T> entry : entityMap.entrySet()) {
            if (entry.getValue().getName().equals(entityName))
                return entry.getValue();
        }
        return null;
    }

    @Nullable
    public T findOne(
            @NotNull final String entityName,
            @NotNull final String userId
    ) {
        return null;
    }

    @Nullable
    public T findOne(
            @NotNull final String entityName,
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return null;
    }

    @Nullable
    @Override
    public T persist(@NotNull final T example) throws InsertExistingEntityException {
        if (entityMap.containsValue(example)) throw new InsertExistingEntityException();
        return entityMap.put(example.getId(), example);
    }

    @NotNull
    @Override
    public Collection<T> persist(@NotNull final Collection<T> collection) {
        if (!entityMap.isEmpty()) entityMap.clear();
        collection.forEach(e -> entityMap.put(e.getId(), e));
        return collection;
    }

    @Nullable
    @Override
    public T merge(@NotNull final T example) throws InsertExistingEntityException {
        if (entityMap.containsValue(example)) throw new InsertExistingEntityException();
        return entityMap.put(example.getId(), example);
    }

    @Nullable
    @Override
    public T remove(@NotNull final String entityName) {
        for (@NotNull final Map.Entry<String, T> entry : entityMap.entrySet()) {
            if (!entry.getValue().getName().equals(entityName)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    @Nullable
    public T remove(
            @NotNull final String entityName,
            @NotNull final String userId
    ) {
        return null;
    }

    @Nullable
    public T remove(
            @NotNull final String name,
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        return null;
    }

    @NotNull
    @Override
    public Collection<T> removeAll() {
        final Collection<T> collection = new LinkedList<>(entityMap.values());
        entityMap.clear();
        return collection;
    }

    @NotNull
    public Collection<T> removeAll(@NotNull final String userId) {
        return Collections.EMPTY_LIST;
    }

    @NotNull
    public Collection<Task> removeAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        return Collections.EMPTY_LIST;
    }

}