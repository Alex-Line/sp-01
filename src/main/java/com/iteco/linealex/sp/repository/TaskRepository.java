package com.iteco.linealex.sp.repository;

import com.iteco.linealex.sp.api.repository.IRepository;
import com.iteco.linealex.sp.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractRepository<Task> implements IRepository<Task> {

    @NotNull
    @Override
    public Collection<Task> findAll(
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        @NotNull final Collection<Task> collection = new ArrayList<>();
        for (@NotNull final Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null || !entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getProjectId() != null && entry.getValue().getProjectId().equals(projectId)) {
                collection.add(entry.getValue());
            }
        }
        return collection;
    }

    @NotNull
    public Collection<Task> findAll(@NotNull final String userId) {
        @NotNull final Collection<Task> collection = new ArrayList<>();
        for (@NotNull final Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getProjectId() != null) continue;
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getUserId().equals(userId))
                collection.add(entry.getValue());
        }
        return collection;
    }

    @Nullable
    @Override
    public Task findOne(
            @NotNull final String taskName,
            @NotNull final String userId
    ) {
        for (@NotNull final Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() != null) continue;
            return entry.getValue();
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOne(
            @NotNull final String taskName,
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        for (@NotNull final Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getProjectId() == null) continue;
            if (entry.getValue().getProjectId().equals(projectId)) continue;
            return entry.getValue();
        }
        return null;
    }

    @Nullable
    @Override
    public Task persist(@NotNull final Task example) {
        if (example.getName() == null) return null;
        if (example.getUserId() == null) return null;
        boolean contains = true;
        if (example.getProjectId() == null) {
            contains = contains(example.getName(), example.getUserId());
        } else {
            contains = contains(example.getProjectId(), example.getName(), example.getUserId());
        }
        if (contains) return null;
        entityMap.put(example.getId(), example);
        return example;
    }

    @Nullable
    @Override
    public Task merge(@NotNull final Task example) {
        if (example.getUserId() == null) return null;
        if (example.getName() == null) return null;
        if (example.getProjectId() == null) return null;
        @Nullable final Task oldTask = findOne(example.getName(), example.getProjectId(), example.getUserId());
        if (oldTask == null) return null;
        oldTask.setName(example.getName());
        oldTask.setDescription(example.getDescription());
        oldTask.setDateStart(example.getDateStart());
        oldTask.setDateFinish(example.getDateFinish());
        oldTask.setProjectId(example.getProjectId());
        oldTask.setUserId(example.getUserId());
        persist(example);
        return oldTask;
    }

    @Nullable
    @Override
    public Task remove(@NotNull final String taskName) {
        for (@NotNull final Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!(entry.getValue().getProjectId() == null)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    @Nullable
    @Override
    public Task remove(
            @NotNull final String taskName,
            @NotNull final String userId
    ) {
        for (@NotNull final Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getName() == null) continue;
            if (entry.getValue().getUserId() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (!(entry.getValue().getProjectId() == null)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    @Nullable
    @Override
    public Task remove(
            @NotNull final String taskName,
            @NotNull final String projectId,
            @NotNull final String userId
    ) {
        for (@NotNull final Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getUserId() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            if (entry.getValue().getProjectId() == null) continue;
            if (!entry.getValue().getProjectId().equals(projectId)) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            return entityMap.remove(entry.getKey());
        }
        return null;
    }

    @NotNull
    @Override
    public Collection<Task> removeAll(@NotNull final String userId) {
        @NotNull final Collection<Task> collection = new ArrayList<>();
        for (@NotNull final Iterator<Map.Entry<String, Task>> iterator = entityMap.entrySet().iterator();
             iterator.hasNext(); ) {
            @NotNull final Map.Entry<String, Task> entry = iterator.next();
            if (entry.getValue().getProjectId() != null) continue;
            if (entry.getValue().getUserId() != null && entry.getValue().getUserId().equals(userId)) {
                collection.add(entry.getValue());
                iterator.remove();
            }
        }
        return collection;
    }

    @NotNull
    @Override
    public Collection<Task> removeAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final Collection<Task> collection = new ArrayList<>();
        for (@NotNull final Iterator<Map.Entry<String, Task>> iterator = entityMap.entrySet().iterator();
             iterator.hasNext(); ) {
            @NotNull final Map.Entry<String, Task> entry = iterator.next();
            if (entry.getValue().getProjectId() == null ||
                    !(entry.getValue().getProjectId().equals(projectId))) continue;
            if (entry.getValue().getUserId() != null && entry.getValue().getUserId().equals(userId)) {
                collection.add(entry.getValue());
                iterator.remove();
            }
        }
        return collection;
    }

    @Override
    public boolean contains(@NotNull final String entityName) {
        return false;
    }

    public boolean contains(
            @NotNull final String taskName,
            @NotNull final String userId
    ) {
        for (@NotNull final Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getProjectId() != null) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getUserId() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            return true;
        }
        return false;
    }

    public boolean contains(
            @NotNull final String projectId,
            @NotNull final String taskName,
            @NotNull final String userId
    ) {
        for (@NotNull final Map.Entry<String, Task> entry : entityMap.entrySet()) {
            if (entry.getValue().getProjectId() != null &&
                    !entry.getValue().getProjectId().equals(projectId)) continue;
            if (entry.getValue().getName() == null) continue;
            if (!entry.getValue().getName().equals(taskName)) continue;
            if (entry.getValue().getUserId() == null) continue;
            if (!entry.getValue().getUserId().equals(userId)) continue;
            return true;
        }
        return false;
    }

    @NotNull
    @Override
    public Collection<Task> findAllByName(
            @NotNull String pattern,
            @NotNull String projectId,
            @NotNull String userId
    ) {
        return entityMap.values().stream().filter((e) ->
                ((e.getProjectId() != null && e.getProjectId().equals(projectId))
                        && (e.getUserId() != null && e.getUserId().equals(userId))
                        && ((e.getName() != null && e.getName().contains(pattern))
                        || (e.getDescription() != null && e.getDescription().contains(pattern)))))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Task> findAllByName(
            @NotNull String pattern,
            @NotNull String userId
    ) {
        return entityMap.values().stream().filter((e) ->
                ((e.getProjectId() == null)
                        && (e.getUserId() != null && e.getUserId().equals(userId))
                        && ((e.getName() != null && e.getName().contains(pattern))
                        || (e.getDescription() != null && e.getDescription().contains(pattern)))))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Task> findAllByName(@NotNull String pattern) {
        return entityMap.values().stream().filter((e) ->
                ((e.getName() != null && e.getName().contains(pattern)) ||
                        (e.getDescription() != null && e.getDescription().contains(pattern))))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStartDate(
            @NotNull String projectId,
            @NotNull String userId
    ) {
        return entityMap.values().stream().filter(e ->
                ((e.getUserId() != null && e.getUserId().equals(userId))
                        && e.getProjectId() != null
                        && e.getProjectId().equals(projectId)
                        && e.getDateStart() != null))
                .sorted(Comparator.comparing(Task::getDateStart))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStartDate(@NotNull String userId) {
        return entityMap.values().stream().filter(e ->
                ((e.getUserId() != null && e.getUserId().equals(userId))
                        && e.getProjectId() == null
                        && e.getDateStart() != null))
                .sorted(Comparator.comparing(Task::getDateStart))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStartDate() {
        return entityMap.values().stream().filter(e -> (e.getDateStart() != null))
                .sorted(Comparator.comparing(Task::getDateStart))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByFinishDate(
            @NotNull String projectId,
            @NotNull String userId
    ) {
        return entityMap.values().stream().filter(e ->
                (e.getUserId() != null
                        && e.getUserId().equals(userId)
                        && e.getProjectId() != null
                        && e.getProjectId().equals(projectId)
                        && e.getDateFinish() != null))
                .sorted(Comparator.comparing(Task::getDateFinish))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByFinishDate(@NotNull String userId) {
        return entityMap.values().stream().filter(e ->
                (e.getUserId() != null
                        && e.getUserId().equals(userId)
                        && e.getProjectId() == null
                        && e.getDateFinish() != null))
                .sorted(Comparator.comparing(Task::getDateFinish))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByFinishDate() {
        return entityMap.values().stream().filter(e -> (e.getDateFinish() != null))
                .sorted(Comparator.comparing(Task::getDateFinish))
                .collect(Collectors.toList());
    }

    @Override
    public @NotNull Collection<Task> findAllSortedByStatus(
            @NotNull String projectId,
            @NotNull String userId
    ) {
        return entityMap.values().stream().filter(e ->
                (e.getUserId() != null
                        && e.getUserId().equals(userId)
                        && e.getProjectId() != null
                        && e.getProjectId().equals(projectId)))
                .sorted(Comparator.comparingInt(o -> o.getStatus().getPriority()))
                .collect(Collectors.toList());
    }

    @Override
    public @NotNull Collection<Task> findAllSortedByStatus(@NotNull String userId) {
        return entityMap.values().stream().filter(e ->
                (e.getUserId() != null
                        && e.getUserId().equals(userId)
                        && e.getProjectId() == null))
                .sorted(Comparator.comparingInt(o -> o.getStatus().getPriority()))
                .collect(Collectors.toList());
    }

    @Override
    public @NotNull Collection<Task> findAllSortedByStatus() {
        return entityMap.values().stream()
                .sorted(Comparator.comparingInt(o -> o.getStatus().getPriority()))
                .collect(Collectors.toList());
    }

}