package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.service.IService;
import com.iteco.linealex.sp.entity.AbstractEntity;
import com.iteco.linealex.sp.exception.InsertExistingEntityException;
import com.iteco.linealex.sp.repository.AbstractRepository;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    protected AbstractRepository<T> repository;

    public AbstractService(@NotNull final AbstractRepository<T> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public Collection<T> getAllEntities() {
        return repository.findAll();
    }

    @NotNull
    public abstract Collection<T> getAllEntitiesByName(@Nullable final String pattern);

    @Nullable
    @Override
    public abstract T persist(@Nullable final T entity) throws InsertExistingEntityException;

    @NotNull
    @Override
    public Collection<T> persist(@NotNull final Collection<T> collection) throws InsertExistingEntityException {
        if (collection.isEmpty()) return Collections.EMPTY_LIST;
        return repository.persist(collection);
    }

    @Nullable
    @Override
    public T removeEntity(@Nullable final String entityName) {
        if (entityName == null || entityName.isEmpty()) return null;
        return repository.remove(entityName);
    }

    @Nullable
    public abstract T removeEntity(
            @Nullable final String entityName,
            @Nullable final String userId);

    @NotNull
    public Collection<T> removeAllEntities(@Nullable final String userId) {
        return new ArrayList<>();
    }

    @NotNull
    public Collection<T> removeAllEntities() {
        return repository.removeAll();
    }

}