package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.repository.ICommandRepository;
import com.iteco.linealex.sp.api.service.ICommandService;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository repository;

    public CommandService(@NotNull final ICommandRepository repository) {
        this.repository = repository;
    }

    @Override
    public void addCommand(@NotNull final AbstractCommand command) {
        repository.addCommand(command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommand(@NotNull final String command) {
        return repository.getCommand(command);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return repository.getAllCommands();
    }

}