package com.iteco.linealex.sp.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getUserByLogin complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getUserByLogin"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://endpoint.api.sp.linealex.iteco.com/}sessionDto" minOccurs="0"/&gt;
 *         &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="selectedUser" type="{http://endpoint.api.sp.linealex.iteco.com/}userDto" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUserByLogin", propOrder = {
        "session",
        "login",
        "selectedUser"
})
public class GetUserByLogin {

    protected SessionDto session;
    protected String login;
    protected UserDto selectedUser;

    /**
     * Gets the value of the session property.
     *
     * @return possible object is
     * {@link SessionDto }
     */
    public SessionDto getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     *
     * @param value allowed object is
     *              {@link SessionDto }
     */
    public void setSession(SessionDto value) {
        this.session = value;
    }

    /**
     * Gets the value of the login property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets the value of the login property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLogin(String value) {
        this.login = value;
    }

    /**
     * Gets the value of the selectedUser property.
     *
     * @return possible object is
     * {@link UserDto }
     */
    public UserDto getSelectedUser() {
        return selectedUser;
    }

    /**
     * Sets the value of the selectedUser property.
     *
     * @param value allowed object is
     *              {@link UserDto }
     */
    public void setSelectedUser(UserDto value) {
        this.selectedUser = value;
    }

}
