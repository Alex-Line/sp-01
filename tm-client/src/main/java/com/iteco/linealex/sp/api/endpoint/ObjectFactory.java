package com.iteco.linealex.sp.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.iteco.linealex.sp.api.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.sp.linealex.iteco.com/", "Exception");
    private final static QName _CreateSession_QNAME = new QName("http://endpoint.api.sp.linealex.iteco.com/", "createSession");
    private final static QName _CreateSessionResponse_QNAME = new QName("http://endpoint.api.sp.linealex.iteco.com/", "createSessionResponse");
    private final static QName _FindSession_QNAME = new QName("http://endpoint.api.sp.linealex.iteco.com/", "findSession");
    private final static QName _FindSessionResponse_QNAME = new QName("http://endpoint.api.sp.linealex.iteco.com/", "findSessionResponse");
    private final static QName _RemoveSession_QNAME = new QName("http://endpoint.api.sp.linealex.iteco.com/", "removeSession");
    private final static QName _RemoveSessionResponse_QNAME = new QName("http://endpoint.api.sp.linealex.iteco.com/", "removeSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.iteco.linealex.sp.api.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CreateSession }
     */
    public CreateSession createCreateSession() {
        return new CreateSession();
    }

    /**
     * Create an instance of {@link CreateSessionResponse }
     */
    public CreateSessionResponse createCreateSessionResponse() {
        return new CreateSessionResponse();
    }

    /**
     * Create an instance of {@link FindSession }
     */
    public FindSession createFindSession() {
        return new FindSession();
    }

    /**
     * Create an instance of {@link FindSessionResponse }
     */
    public FindSessionResponse createFindSessionResponse() {
        return new FindSessionResponse();
    }

    /**
     * Create an instance of {@link RemoveSession }
     */
    public RemoveSession createRemoveSession() {
        return new RemoveSession();
    }

    /**
     * Create an instance of {@link RemoveSessionResponse }
     */
    public RemoveSessionResponse createRemoveSessionResponse() {
        return new RemoveSessionResponse();
    }

    /**
     * Create an instance of {@link SessionDto }
     */
    public SessionDto createSessionDto() {
        return new SessionDto();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.sp.linealex.iteco.com/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSession }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CreateSession }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.sp.linealex.iteco.com/", name = "createSession")
    public JAXBElement<CreateSession> createCreateSession(CreateSession value) {
        return new JAXBElement<CreateSession>(_CreateSession_QNAME, CreateSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSessionResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link CreateSessionResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.sp.linealex.iteco.com/", name = "createSessionResponse")
    public JAXBElement<CreateSessionResponse> createCreateSessionResponse(CreateSessionResponse value) {
        return new JAXBElement<CreateSessionResponse>(_CreateSessionResponse_QNAME, CreateSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindSession }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindSession }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.sp.linealex.iteco.com/", name = "findSession")
    public JAXBElement<FindSession> createFindSession(FindSession value) {
        return new JAXBElement<FindSession>(_FindSession_QNAME, FindSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindSessionResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link FindSessionResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.sp.linealex.iteco.com/", name = "findSessionResponse")
    public JAXBElement<FindSessionResponse> createFindSessionResponse(FindSessionResponse value) {
        return new JAXBElement<FindSessionResponse>(_FindSessionResponse_QNAME, FindSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSession }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link RemoveSession }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.sp.linealex.iteco.com/", name = "removeSession")
    public JAXBElement<RemoveSession> createRemoveSession(RemoveSession value) {
        return new JAXBElement<RemoveSession>(_RemoveSession_QNAME, RemoveSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSessionResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link RemoveSessionResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.sp.linealex.iteco.com/", name = "removeSessionResponse")
    public JAXBElement<RemoveSessionResponse> createRemoveSessionResponse(RemoveSessionResponse value) {
        return new JAXBElement<RemoveSessionResponse>(_RemoveSessionResponse_QNAME, RemoveSessionResponse.class, null, value);
    }

}
