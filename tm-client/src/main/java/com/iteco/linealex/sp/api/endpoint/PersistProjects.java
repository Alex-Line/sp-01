package com.iteco.linealex.sp.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for persistProjects complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="persistProjects"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://endpoint.api.sp.linealex.iteco.com/}sessionDto" minOccurs="0"/&gt;
 *         &lt;element name="projects" type="{http://endpoint.api.sp.linealex.iteco.com/}projectDto" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "persistProjects", propOrder = {
        "session",
        "projects"
})
public class PersistProjects {

    protected SessionDto session;
    protected List<ProjectDto> projects;

    /**
     * Gets the value of the session property.
     *
     * @return possible object is
     * {@link SessionDto }
     */
    public SessionDto getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     *
     * @param value allowed object is
     *              {@link SessionDto }
     */
    public void setSession(SessionDto value) {
        this.session = value;
    }

    /**
     * Gets the value of the projects property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the projects property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProjects().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProjectDto }
     */
    public List<ProjectDto> getProjects() {
        if (projects == null) {
            projects = new ArrayList<ProjectDto>();
        }
        return this.projects;
    }

}
