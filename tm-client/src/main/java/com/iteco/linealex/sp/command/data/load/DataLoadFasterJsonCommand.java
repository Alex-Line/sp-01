package com.iteco.linealex.sp.command.data.load;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component(value = "data-load-faster-json")
public class DataLoadFasterJsonCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-load-faster-json";
    }

    @NotNull
    @Override
    public String description() {
        return "LOAD DATA FROM JSON FILE BY FASTER_XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("LOADING DATA FROM JSON FILE");
        bootstrap.getUserEndpoint().loadFasterJson(bootstrap.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}