package com.iteco.linealex.sp.command.data.load;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component(value = "data-load-jaxb-xml")
public class DataLoadJaxBFromXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-load-jaxb-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "LOAD DATA FROM XML FILE BY JAXB";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("LOADING DATA FROM XML FILE");
        bootstrap.getUserEndpoint().loadJaxbXml(bootstrap.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}