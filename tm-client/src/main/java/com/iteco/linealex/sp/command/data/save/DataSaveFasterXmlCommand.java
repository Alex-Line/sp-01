package com.iteco.linealex.sp.command.data.save;

import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component(value = "data-save-faster-xml")
public class DataSaveFasterXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-save-faster-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "SAVE DATA INTO XML FILE BY FASTER-XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SAVING DATA TO XML FILE");
        bootstrap.getUserEndpoint().saveFasterXml(bootstrap.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}