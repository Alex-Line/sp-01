package com.iteco.linealex.sp.command.project;

import com.iteco.linealex.sp.api.endpoint.ProjectDto;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component(value = "project-create")
public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "CREATE A NEW PROJECT AND SET SELECTION ON IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        @Nullable final ProjectDto project = new ProjectDto();
        project.setUserId(session.getUserId());
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = bootstrap.getTerminalService().nextLine();
        project.setName(projectName);
        System.out.println("[ENTER PROJECT DESCRIPTION]");
        @NotNull final String projectDescription = bootstrap.getTerminalService().nextLine();
        project.setDescription(projectDescription);
        System.out.println("[ENTER PROJECT START DATE IN FORMAT: DD.MM.YYYY]");
        project.setDateStart(ApplicationUtils.toXMLGregorianCalendar(ApplicationUtils.formatStringToDate(
                bootstrap.getTerminalService().nextLine())));
        System.out.println("[ENTER PROJECT FINISH DATE IN FORMAT: DD.MM.YYYY]");
        project.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(ApplicationUtils.formatStringToDate(
                bootstrap.getTerminalService().nextLine())));
        bootstrap.getProjectEndpoint().persistProject(session, project);
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}