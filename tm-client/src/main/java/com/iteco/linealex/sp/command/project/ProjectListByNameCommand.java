package com.iteco.linealex.sp.command.project;

import com.iteco.linealex.sp.api.endpoint.ProjectDto;
import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Component(value = "project-list-name")
public final class ProjectListByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list-name";
    }

    @NotNull
    @Override
    public String description() {
        return "LIST PROJECTS BY PART OF NAME OR DESCRIPTION";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        System.out.println("ENTER PART OF NAME OR DESCRIPTION TO SEARCH");
        @NotNull final String pattern = bootstrap.getTerminalService().nextLine();
        System.out.println("[PROJECT LIST]");
        @NotNull Collection<ProjectDto> collection = Collections.EMPTY_LIST;
        if (session.getRole() == Role.ADMINISTRATOR) {
            collection = bootstrap.getProjectEndpoint().getAllProjectsByName(session, pattern);
        } else collection = bootstrap.getProjectEndpoint()
                .getAllProjectsByNameWithUserId(session, session.getUserId(), pattern);
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY PROJECTS]\n");
            return;
        }
        int index = 1;
        for (@NotNull final ProjectDto project : collection) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}