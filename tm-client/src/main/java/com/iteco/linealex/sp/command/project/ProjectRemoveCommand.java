package com.iteco.linealex.sp.command.project;

import com.iteco.linealex.sp.api.endpoint.ProjectDto;
import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component(value = "project-remove")
public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVING A PROJECT BY NAME";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = bootstrap.getTerminalService().nextLine();
        @Nullable ProjectDto project = null;
        if (session.getRole() == Role.ADMINISTRATOR)
            project = bootstrap.getProjectEndpoint().getProjectByName(session, projectName);
        else project = bootstrap.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        if (project == null) {
            System.out.println("[THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!]\n");
            return;
        }
        bootstrap.getProjectEndpoint().removeProject(session, project.getId());
        System.out.println("[REMOVING PROJECT]");
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(session, session.getUserId(), project.getId());
        System.out.println("[REMOVING TASKS FROM PROJECT]");
        System.out.println("[ALL TASKS REMOVED]");
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}