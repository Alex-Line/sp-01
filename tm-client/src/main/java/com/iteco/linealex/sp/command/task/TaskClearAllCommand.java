package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component(value = "task-clear-all")
public final class TaskClearAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-clear-all";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVE ALL TASK EVERYWHERE";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        bootstrap.getTaskEndpoint().removeAllTasksWithUserId(session, session.getUserId());
        System.out.println("[ALL TASKS FROM ALL PROJECTS ARE REMOVED]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}