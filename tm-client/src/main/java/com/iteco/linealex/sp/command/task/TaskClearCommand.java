package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.api.endpoint.ProjectDto;
import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component(value = "task-clear")
public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVE ALL TASKS FROM THE SELECTED PROJECT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = bootstrap.getTerminalService().nextLine();
        System.out.println("[ENTER TASK NAME]");
        @Nullable ProjectDto project = null;
        if (session.getRole() == Role.ADMINISTRATOR) project = bootstrap.getProjectEndpoint()
                .getProjectByName(session, projectName);
        else project = bootstrap.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        if (project == null)
            System.out.println("[YOU DID NOT SELECT ANY PROJECT! SELECT ANY AND TRY AGAIN]\n");
        else bootstrap.getTaskEndpoint().removeAllTasksFromProject(session, session.getUserId(), project.getId());
        System.out.println("[REMOVING TASKS FROM PROJECT]");
        System.out.println("[ALL TASKS REMOVED]");
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}