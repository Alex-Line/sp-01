package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.api.endpoint.ProjectDto;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.TaskDto;
import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component(value = "task-create")
public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "CREATE A NEW TASK IN PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = bootstrap.getTerminalService().nextLine();
        @Nullable final ProjectDto project = bootstrap.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        @NotNull final TaskDto task = new TaskDto();
        task.setUserId(session.getUserId());
        System.out.println("[ENTER TASK NAME]");
        @NotNull final String taskName = bootstrap.getTerminalService().nextLine();
        task.setName(taskName);
        System.out.println("[ENTER TASK DESCRIPTION]");
        @NotNull final String taskDescription = bootstrap.getTerminalService().nextLine();
        task.setDescription(taskDescription);
        System.out.println("[ENTER TASK START DATE IN FORMAT: DD.MM.YYYY]");
        task.setDateStart(ApplicationUtils.toXMLGregorianCalendar(
                ApplicationUtils.formatStringToDate(bootstrap.getTerminalService().nextLine())));
        System.out.println("[ENTER TASK FINISH DATE IN FORMAT: DD.MM.YYYY]");
        task.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(
                ApplicationUtils.formatStringToDate(bootstrap.getTerminalService().nextLine())));
        if (project == null) task.setProjectId(null);
        else task.setProjectId(project.getId());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}