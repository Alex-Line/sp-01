package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.api.endpoint.*;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import java.lang.Exception;
import java.util.Collection;
import java.util.Collections;

@Component(value = "task-list-finish")
public class TaskListByFinishDateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list-finish";
    }

    @NotNull
    @Override
    public String description() {
        return "LIST TASKS BY FINISH DATE";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        @Nullable final UserDto selectedUser = bootstrap.getUserEndpoint().getUserById(session, session.getUserId());
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = bootstrap.getTerminalService().nextLine();
        @Nullable final ProjectDto selectedProject = bootstrap.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        System.out.println("[TASK LIST]");
        @NotNull Collection<TaskDto> collection = Collections.EMPTY_LIST;
        if (selectedUser.getRole() == Role.ADMINISTRATOR)
            collection = bootstrap.getTaskEndpoint().getAllTasksSortedByFinishDate(session);
        else if (selectedProject != null)
            collection = bootstrap.getTaskEndpoint()
                    .getAllTasksSortedByFinishDateWithUserIdAndProjectId(session, session.getUserId(), selectedProject.getId());
        else collection = bootstrap.getTaskEndpoint()
                    .getAllTasksSortedByFinishDateWithUserId(session, session.getUserId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY PROJECTS]\n");
            return;
        }
        int index = 1;
        for (@NotNull final TaskDto task : collection) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}