package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.api.endpoint.*;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import java.lang.Exception;
import java.util.Collection;
import java.util.Collections;

@Component(value = "task-list-name")
public class TaskListByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list-name";
    }

    @NotNull
    @Override
    public String description() {
        return "TASK LIST BY PART OF NAME OR DESCRIPTION";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        @Nullable final UserDto selectedUser = bootstrap.getUserEndpoint().getUserById(session, session.getUserId());
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = bootstrap.getTerminalService().nextLine();
        @Nullable final ProjectDto selectedProject = bootstrap.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        System.out.println("ENTER PART OF NAME OR DESCRIPTION TO SEARCH");
        @NotNull final String pattern = bootstrap.getTerminalService().nextLine();
        System.out.println("[PROJECT LIST]");
        @NotNull Collection<TaskDto> collection = Collections.EMPTY_LIST;
        if (selectedUser.getRole() == Role.ADMINISTRATOR)
            collection = bootstrap.getTaskEndpoint().getAllTasksByName(session, pattern);
        else if (selectedProject != null)
            collection = bootstrap.getTaskEndpoint()
                    .getAllTasksByNameWithUserIdAndProjectId(session, session.getUserId(), selectedProject.getId(), pattern);
        else collection = bootstrap.getTaskEndpoint()
                    .getAllTasksByNameWithUserId(session, selectedUser.getId(), pattern);
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY PROJECTS]\n");
            return;
        }
        int index = 1;
        for (@NotNull final TaskDto task : collection) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return false;
    }

}