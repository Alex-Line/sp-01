package com.iteco.linealex.sp.command.task;

import com.iteco.linealex.sp.api.endpoint.ProjectDto;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.TaskDto;
import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component(value = "task-remove")
public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "REMOVE ONE TASK IN SELECTED PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        System.out.println("[ENTER PROJECT NAME]");
        @NotNull final String projectName = bootstrap.getTerminalService().nextLine();
        @Nullable final ProjectDto selectedProject = bootstrap.getProjectEndpoint()
                .getProjectByNameWithUserId(session, session.getUserId(), projectName);
        System.out.println("[ENTER TASK NAME]");
        @NotNull final String taskName = bootstrap.getTerminalService().nextLine();
        System.out.println("REMOVING TASK...");
        @Nullable TaskDto task = null;
        if (selectedProject != null) {
            task = bootstrap.getTaskEndpoint()
                    .getTaskByNameWithUserIdAndProjectId(session, session.getUserId(), selectedProject.getId(), taskName);
        } else task = bootstrap.getTaskEndpoint()
                .getTaskByNameWithUserId(session, session.getUserId(), taskName);
        if (task == null) {
            System.out.println("[THERE IS NOT SUCH TASK AS " + taskName
                    + ". PLEASE TRY AGAIN OR SELECT PROJECT FIRST]\n");
            return;
        }
        bootstrap.getTaskEndpoint().removeTask(session, task.getId());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}