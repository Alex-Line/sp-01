package com.iteco.linealex.sp.command.user;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component(value = "user-create")
public final class UserCreateCommand extends AbstractCommand {

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    @Override
    public String command() {
        return "user-create";
    }

    @NotNull
    @Override
    public String description() {
        return "REGISTER NEW USER IF THEY DO NOT EXIST. (AVAILABLE FOR AMDMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER LOGIN. IT MUST BE UNIQUE!");
        @NotNull final String login = bootstrap.getTerminalService().nextLine();
        System.out.println("ENTER USER PASSWORD. IT MUST BE 8 DIGITS OR LONGER");
        @NotNull final String password = bootstrap.getTerminalService().nextLine();
        if (password.length() < 8) throw new TaskManagerException_Exception();
        @NotNull final String hashPassword = TransformatorToHashMD5.getHash(password,
                passwordSalt, Integer.parseInt(passwordTimes));
        @NotNull final UserDto selectedUser = bootstrap.getUserEndpoint().getUserById(bootstrap.getSession(),
                bootstrap.getSession().getUserId());
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin(login);
        newUser.setHashPassword(hashPassword);
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().createUser(bootstrap.getSession(), newUser, selectedUser);
        System.out.println("[WAS REGISTERED NEW USER]");
        printUser(newUser);
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}