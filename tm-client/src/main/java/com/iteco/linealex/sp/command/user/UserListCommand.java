package com.iteco.linealex.sp.command.user;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component(value = "user-list")
public final class UserListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-list";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ALL USERS REGISTERED IN THE TASK MANAGER. (AVAILABLE FOR AMDMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        @Nullable final UserDto selectedUser = bootstrap.getUserEndpoint()
                .getUserById(session, session.getUserId());
        if (selectedUser == null) throw new TaskManagerException_Exception();
        if (session.getRole() != Role.ADMINISTRATOR)
            throw new TaskManagerException_Exception();
        @NotNull final Collection<UserDto> collection = bootstrap.getUserEndpoint().getAllUsers(session);
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY USERS YET]\n");
            return;
        }
        int index = 1;
        for (@NotNull final UserDto user : collection) {
            System.out.print(index + ". ");
            printUser(user);
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}