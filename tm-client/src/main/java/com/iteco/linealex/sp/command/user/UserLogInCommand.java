package com.iteco.linealex.sp.command.user;

import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component(value = "login")
public final class UserLogInCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "SIGN IN INTO YOUR ACCOUNT";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER LOGIN");
        @NotNull final String login = bootstrap.getTerminalService().nextLine();
        System.out.println("ENTER USER PASSWORD");
        @NotNull final String password = bootstrap.getTerminalService().nextLine();
        @Nullable final SessionDto session = bootstrap.getSessionEndpoint().createSession(login, password);
        @Nullable final UserDto user = bootstrap.getUserEndpoint().logInUser(login, password);
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        bootstrap.setSession(session);
        System.out.println("[USER " + user.getLogin() + " ENTERED TO TASK MANAGER]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}