package com.iteco.linealex.sp.command.user;

import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component(value = "logout")
public final class UserLogOutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "SIGN OUT OF YOUR ACCOUNT IF IT WAS SIGN IN";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        if (session == null) {
            System.out.println("[YOU HAVE NOT BEEN SIGN IN]\n");
            return;
        }
        bootstrap.getUserEndpoint().logOutUser(session);
        bootstrap.setSession(null);
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}