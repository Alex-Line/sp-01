package com.iteco.linealex.sp.command.user;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component(value = "user-show")
public final class UserShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-show";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ANY USER BY LOGIN. (AVAILABLE FOR ADMINS ONLY)";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        if (session == null) {
            System.out.println("[YOU MUST BE AUTHORISED TO DO THAT]\n");
            return;
        }
        @Nullable final UserDto selectedUser = bootstrap.getUserEndpoint().getUserById(session, session.getUserId());
        if (selectedUser == null) throw new TaskManagerException_Exception();
        System.out.println("ENTER USER LOGIN");
        @NotNull final String login = bootstrap.getTerminalService().nextLine();
        if (session.getRole() == Role.ADMINISTRATOR) {
            @Nullable final UserDto user = bootstrap.getUserEndpoint().getUserByLogin(session, login, selectedUser);
            printUser(user);
            System.out.println();
            return;
        }
        if (login.equals(selectedUser.getLogin())) {
            printUser(selectedUser);
            System.out.println();
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}