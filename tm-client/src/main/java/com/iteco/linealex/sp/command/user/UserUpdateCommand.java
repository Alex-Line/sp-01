package com.iteco.linealex.sp.command.user;

import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component(value = "user-update")
public final class UserUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-update";
    }

    @NotNull
    @Override
    public String description() {
        return "UPDATE USER'S PASSWORD";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = bootstrap.getSession();
        if (session == null) throw new TaskManagerException_Exception();
        @Nullable final UserDto user = bootstrap.getUserEndpoint().getUserById(session, session.getUserId());
        System.out.println("ENTER OLD PASSWORD");
        @NotNull final String oldPassword = bootstrap.getTerminalService().nextLine();
        System.out.println("ENTER NEW PASSWORD");
        @NotNull final String newPassword = bootstrap.getTerminalService().nextLine();
        System.out.println("ENTER NEW PASSWORD AGAIN");
        if (newPassword.equals(bootstrap.getTerminalService().nextLine())) {
            bootstrap.getUserEndpoint().updateUserPassword(session, oldPassword, newPassword, user);
            System.out.println("[OK]\n");
        } else System.out.println("MISTAKE IN THE REPEATING OF NEW PASSWORD\n");
    }

    @Override
    public boolean secure() {
        return true;
    }

}