package com.iteco.linealex.sp.config;

import com.iteco.linealex.sp.api.endpoint.IProjectEndpoint;
import com.iteco.linealex.sp.api.endpoint.ISessionEndpoint;
import com.iteco.linealex.sp.api.endpoint.ITaskEndpoint;
import com.iteco.linealex.sp.api.endpoint.IUserEndpoint;
import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.endpoint.ProjectEndpointService;
import com.iteco.linealex.sp.endpoint.SessionEndpointService;
import com.iteco.linealex.sp.endpoint.TaskEndpointService;
import com.iteco.linealex.sp.endpoint.UserEndpointService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = "com.iteco.linealex.sp")
public class ApplicationConfig {

    @Bean
    public Bootstrap bootstrap() {
        return new Bootstrap();
    }

    @Bean
    public ISessionEndpoint sessionEndpoint() {
        return new SessionEndpointService().getSessionEndpointPort();
    }

    @Bean
    public IProjectEndpoint projectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    public ITaskEndpoint taskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    public IUserEndpoint userEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }

    @Bean("commands")
    public Map<String, AbstractCommand> commands() {
        return new HashMap<>();
    }

}