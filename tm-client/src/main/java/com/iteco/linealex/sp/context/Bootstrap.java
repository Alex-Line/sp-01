package com.iteco.linealex.sp.context;

import com.iteco.linealex.sp.api.endpoint.*;
import com.iteco.linealex.sp.command.AbstractCommand;
import com.iteco.linealex.sp.service.TerminalService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.Exception;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

@Getter
@Setter
@Component
@NoArgsConstructor
public class Bootstrap {

    @NotNull
    @Autowired
    private TerminalService terminalService;

    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    @Nullable
    private SessionDto session;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    @Qualifier("commands")
    private Map<String, AbstractCommand> commands;

    public void start() {
        try {
            System.out.println("*** WELCOME TO TASK MANAGER ***");
            @NotNull String command = "";
            while (!"exit".equals(command)) {
                command = terminalService.nextLine().toLowerCase();
                try {
                    execute(command);
                } catch (TaskManagerException_Exception taskException) {
                    System.out.println(taskException.getMessage());
                }
            }
        } catch (TaskManagerException_Exception | NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

}