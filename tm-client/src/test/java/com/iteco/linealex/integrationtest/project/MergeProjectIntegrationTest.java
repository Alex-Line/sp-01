package com.iteco.linealex.integrationtest.project;

import com.iteco.linealex.sp.api.endpoint.*;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.lang.Exception;
import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MergeProjectIntegrationTest {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void mergeProjectPositive1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest");
        projectTest.setDescription("projectTest");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        @NotNull ProjectDto returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(adminSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        projectTest.setStatus(Status.PROCESSING);
        bootstrap.getProjectEndpoint().mergeProject(adminSession, projectTest);
        returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(adminSession, projectTest.getId());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void mergeProjectPositive2() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest");
        projectTest.setDescription("projectTest");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        @NotNull ProjectDto returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(adminSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        projectTest.setDescription("different description");
        bootstrap.getProjectEndpoint().mergeProject(adminSession, projectTest);
        returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(adminSession, projectTest.getId());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void mergeProjectPositive3() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest");
        projectTest.setDescription("projectTest");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        @NotNull ProjectDto returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(adminSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        projectTest.setName("different name");
        bootstrap.getProjectEndpoint().mergeProject(adminSession, projectTest);
        returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(adminSession, projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void mergeProjectPositive4() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("project2");
        projectTest.setDescription("project2");
        projectTest.setStatus(Status.PROCESSING);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        @NotNull ProjectDto returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(userSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        projectTest.setName("different name");
        bootstrap.getProjectEndpoint().mergeProject(userSession, projectTest);
        returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(userSession, projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void mergeProjectPositive5() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("project2");
        projectTest.setDescription("project2");
        projectTest.setStatus(Status.PROCESSING);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        @NotNull ProjectDto returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(userSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        projectTest.setDescription("different description");
        bootstrap.getProjectEndpoint().mergeProject(userSession, projectTest);
        returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(userSession, projectTest.getId());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void mergeProjectPositive6() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("project2");
        projectTest.setDescription("project2");
        projectTest.setStatus(Status.PROCESSING);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        @NotNull ProjectDto returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(userSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        projectTest.setDescription("different description");
        bootstrap.getProjectEndpoint().mergeProject(adminSession, projectTest);
        returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(userSession, projectTest.getId());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void mergeProjectNegative1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("project4");
        projectTest.setDescription("project4");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        @NotNull ProjectDto returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(adminSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        projectTest.setDescription("alternative description");
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getProjectEndpoint().mergeProject(userSession, projectTest);
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

    @Test
    void mergeProjectNegative2() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("project4");
        projectTest.setDescription("project4");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB project
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        @NotNull ProjectDto returnedProject = bootstrap.getProjectEndpoint().
                getProjectById(adminSession, projectTest.getId());
        assertEquals(returnedProject.getId(), projectTest.getId());
        assertEquals(returnedProject.getName(), projectTest.getName());
        assertEquals(returnedProject.getDescription(), projectTest.getDescription());
        assertEquals(returnedProject.getStatus(), projectTest.getStatus());
        assertEquals(returnedProject.getUserId(), projectTest.getUserId());
        projectTest.setDescription("alternative description");
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getProjectEndpoint().mergeProject(adminSession, null);
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

}