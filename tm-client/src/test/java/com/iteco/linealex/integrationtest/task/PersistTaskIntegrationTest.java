package com.iteco.linealex.integrationtest.task;

import com.iteco.linealex.sp.api.endpoint.*;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.lang.Exception;
import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PersistTaskIntegrationTest {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void persistTaskPositive1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Creating tasks
         */
        @NotNull final TaskDto taskTest = new TaskDto();
        taskTest.setUserId(admin.getId());
        taskTest.setProjectId(projectTest.getId());
        taskTest.setId(UUID.randomUUID().toString());
        taskTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setName("taskTest1");
        taskTest.setDescription("taskTest1");
        taskTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB task
         */
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        bootstrap.getTaskEndpoint().persistTask(adminSession, taskTest);

        @NotNull TaskDto returnedTask = bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest.getId());
        assertNotNull(returnedTask);
        assertEquals(returnedTask.getId(), taskTest.getId());
        assertEquals(returnedTask.getProjectId(), taskTest.getProjectId());
        assertEquals(returnedTask.getStatus(), taskTest.getStatus());
        assertEquals(returnedTask.getUserId(), taskTest.getUserId());
        assertEquals(returnedTask.getProjectId(), taskTest.getProjectId());
        assertEquals(returnedTask.getName(), taskTest.getName());
        assertEquals(returnedTask.getDescription(), taskTest.getDescription());
        /**
         * Clean up
         */
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(adminSession, admin.getId(), projectTest.getId());
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void persistTaskPositive2() throws Exception {
        /**
         * Registration for user
         */
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(user.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Creating tasks
         */
        @NotNull final TaskDto taskTest = new TaskDto();
        taskTest.setUserId(user.getId());
        taskTest.setProjectId(projectTest.getId());
        taskTest.setId(UUID.randomUUID().toString());
        taskTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setName("taskTest1");
        taskTest.setDescription("taskTest1");
        taskTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB task
         */
        bootstrap.getProjectEndpoint().persistProject(userSession, projectTest);
        bootstrap.getTaskEndpoint().persistTask(userSession, taskTest);

        @NotNull TaskDto returnedTask = bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest.getId());
        assertNotNull(returnedTask);
        assertEquals(returnedTask.getId(), taskTest.getId());
        assertEquals(returnedTask.getProjectId(), taskTest.getProjectId());
        assertEquals(returnedTask.getStatus(), taskTest.getStatus());
        assertEquals(returnedTask.getUserId(), taskTest.getUserId());
        assertEquals(returnedTask.getProjectId(), taskTest.getProjectId());
        assertEquals(returnedTask.getName(), taskTest.getName());
        assertEquals(returnedTask.getDescription(), taskTest.getDescription());
        /**
         * Clean up
         */
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(userSession, user.getId(), projectTest.getId());
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(userSession, projectTest.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(userSession, taskTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(userSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void persistTaskNegative1() throws Exception {
        /**
         * Registration for user
         */
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        /**
         * Creating a new project
         */
        @NotNull final ProjectDto projectTest = new ProjectDto();
        projectTest.setUserId(admin.getId());
        projectTest.setId(UUID.randomUUID().toString());
        projectTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        projectTest.setName("projectTest1");
        projectTest.setDescription("projectTest1");
        projectTest.setStatus(Status.PLANNED);
        /**
         * Creating tasks
         */
        @NotNull final TaskDto taskTest = new TaskDto();
        taskTest.setUserId(admin.getId());
        taskTest.setProjectId(projectTest.getId());
        taskTest.setId(UUID.randomUUID().toString());
        taskTest.setDateStart(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setDateFinish(ApplicationUtils.toXMLGregorianCalendar(new Date()));
        taskTest.setName("taskTest1");
        taskTest.setDescription("taskTest1");
        taskTest.setStatus(Status.PLANNED);
        /**
         * Inserting project and check returned from DB task
         */
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.getProjectEndpoint().persistProject(adminSession, projectTest);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getTaskEndpoint().persistTask(userSession, taskTest);
        });
        assertNotNull(thrown.getMessage());
        /**
         * Clean up
         */
        bootstrap.getTaskEndpoint().removeAllTasksFromProject(adminSession, admin.getId(), projectTest.getId());
        bootstrap.getProjectEndpoint().removeAllProjects(adminSession);
        assertNull(bootstrap.getProjectEndpoint().getProjectById(adminSession, projectTest.getId()));
        assertNull(bootstrap.getTaskEndpoint().getTaskById(adminSession, taskTest.getId()));
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}