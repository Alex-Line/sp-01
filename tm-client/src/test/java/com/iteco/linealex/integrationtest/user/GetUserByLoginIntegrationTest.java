package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GetUserByLoginIntegrationTest {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void getUserByLoginNegative1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt, Integer.parseInt(passwordTimes)));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserByLogin(null, newUser.getLogin(), admin);
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void getUserByLoginNegative2() throws Exception {
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(userSession.getRole(), Role.ORDINARY_USER);
        assertEquals(userSession.getUserId(), user.getId());
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt, Integer.parseInt(passwordTimes)));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserByLogin(userSession, newUser.getLogin(), user);
        });
        assertNotNull(thrown.getMessage());
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}