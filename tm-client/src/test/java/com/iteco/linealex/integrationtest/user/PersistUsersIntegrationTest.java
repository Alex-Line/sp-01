package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PersistUsersIntegrationTest {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void persistUsersPositive1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        assertSame(adminSession.getRole(), Role.ADMINISTRATOR);
        assertEquals(adminSession.getUserId(), admin.getId());
        @NotNull final List<UserDto> collection = new ArrayList<>();
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt, Integer.parseInt(passwordTimes)));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        @NotNull final UserDto newUser2 = new UserDto();
        newUser2.setLogin("test2");
        newUser2.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt, Integer.parseInt(passwordTimes)));
        newUser2.setRole(Role.ORDINARY_USER);
        newUser2.setId(UUID.randomUUID().toString());
        collection.add(admin);
        collection.add(user);
        collection.add(newUser);
        collection.add(newUser2);
        bootstrap.getUserEndpoint().persistUsers(adminSession, collection);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        @NotNull final List<UserDto> resultList = bootstrap.getUserEndpoint().getAllUsers(adminSession);
        resultList.sort(Comparator.comparing(UserDto::getLogin));
        assertEquals(resultList.get(1).getId(), newUser.getId());
        assertEquals(resultList.get(2).getId(), newUser2.getId());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser2.getId());
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserById(adminSession, newUser.getId());
        });
        assertNotNull(thrown.getMessage());
        @Nullable final Throwable thrown1 = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserById(adminSession, newUser2.getId());
        });
        assertNotNull(thrown1.getMessage());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void persistUsersNegative1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        assertSame(adminSession.getRole(), Role.ADMINISTRATOR);
        assertEquals(adminSession.getUserId(), admin.getId());
        @NotNull final List<UserDto> collection = new ArrayList<>();
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt, Integer.parseInt(passwordTimes)));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        @NotNull final UserDto newUser2 = new UserDto();
        newUser2.setLogin("test2");
        newUser2.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt, Integer.parseInt(passwordTimes)));
        newUser2.setRole(Role.ORDINARY_USER);
        newUser2.setId(UUID.randomUUID().toString());
        collection.add(admin);
        collection.add(user);
        collection.add(newUser);
        collection.add(newUser2);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().persistUsers(userSession, collection);
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

    @Test
    void checkLoginCommandNegative2() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        assertSame(adminSession.getRole(), Role.ADMINISTRATOR);
        assertEquals(adminSession.getUserId(), admin.getId());
        @NotNull final List<UserDto> collection = new ArrayList<>();
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt, Integer.parseInt(passwordTimes)));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        @NotNull final UserDto newUser2 = new UserDto();
        newUser2.setLogin("test2");
        newUser2.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt, Integer.parseInt(passwordTimes)));
        newUser2.setRole(Role.ORDINARY_USER);
        newUser2.setId(UUID.randomUUID().toString());
        collection.add(admin);
        collection.add(user);
        collection.add(newUser);
        collection.add(newUser2);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().persistUsers(null, collection);
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}