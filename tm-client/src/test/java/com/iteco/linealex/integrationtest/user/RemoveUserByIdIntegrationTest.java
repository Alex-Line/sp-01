package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.User;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RemoveUserByIdIntegrationTest {
    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private SessionDto adminSession;

    @Test
    void removeAllUsersByUserIdPositive1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt,
                Integer.parseInt(passwordTimes)));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().createUser(bootstrap.getSession(), newUser, admin);
        @NotNull UserDto result = bootstrap.getUserEndpoint().getUserById(bootstrap.getSession(), newUser.getId());
        assertEquals(result.getId(), newUser.getId());
        assertEquals(result.getHashPassword(), newUser.getHashPassword());
        assertEquals(result.getLogin(), newUser.getLogin());
        assertEquals(result.getRole(), newUser.getRole());
        bootstrap.getUserEndpoint().removeUserById(bootstrap.getSession(), newUser.getId());
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserById(bootstrap.getSession(), newUser.getId());
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void removeAllUsersByUserIdNegative1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        @NotNull final User newUser = new User();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt,
                Integer.parseInt(passwordTimes)));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserById(bootstrap.getSession(), newUser.getId());
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().removeUserById(bootstrap.getSession(), newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}