package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.context.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SaveFasterJsonIntegrationTest {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void saveFasterJsonPositive1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(bootstrap.getSession().getRole(), Role.ADMINISTRATOR);
        assertEquals(bootstrap.getSession().getUserId(), admin.getId());
        bootstrap.getUserEndpoint().saveFasterJson(adminSession);
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void saveFasterJsonPositive2() throws Exception {
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(bootstrap.getSession().getRole(), Role.ORDINARY_USER);
        assertEquals(bootstrap.getSession().getUserId(), user.getId());
        bootstrap.getUserEndpoint().saveFasterJson(userSession);
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

    @Test
    void saveFasterJsonNegative1() throws Exception {
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().saveFasterJson(null);
        });
        assertNotNull(thrown.getMessage());
    }

}