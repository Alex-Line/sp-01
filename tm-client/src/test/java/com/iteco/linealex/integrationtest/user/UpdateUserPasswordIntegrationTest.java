package com.iteco.linealex.integrationtest.user;

import com.iteco.linealex.sp.api.endpoint.Role;
import com.iteco.linealex.sp.api.endpoint.SessionDto;
import com.iteco.linealex.sp.api.endpoint.UserDto;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import java.util.UUID;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UpdateUserPasswordIntegrationTest {

    @Value("ADMIN_PASSWORD")
    String adminPassword;

    @Value("USER_PASSWORD")
    String userPassword;

    @Value("PASSWORD_SALT")
    String passwordSalt;

    @Value("PASSWORD_TIMES")
    String passwordTimes;

    @NotNull
    private final Bootstrap bootstrap = new Bootstrap();

    @NotNull
    private UserDto admin;

    @NotNull
    private UserDto user;

    @NotNull
    private SessionDto adminSession;

    @NotNull
    private SessionDto userSession;

    @Test
    void updateUserPositive1() throws Exception {
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.setSession(adminSession);
        assertSame(adminSession.getRole(), Role.ADMINISTRATOR);
        assertEquals(adminSession.getUserId(), admin.getId());
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt,
                Integer.parseInt(passwordTimes)));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable UserDto returnedUser = bootstrap.getUserEndpoint().getUserById(adminSession, newUser.getId());
        assertNotNull(returnedUser);
        bootstrap.getUserEndpoint()
                .updateUserPassword(adminSession, "12345678", "87654321", newUser);
        newUser.setHashPassword(TransformatorToHashMD5.getHash("87654321",
                passwordSalt,
                Integer.parseInt(passwordTimes)));
        returnedUser = bootstrap.getUserEndpoint().getUserById(adminSession, newUser.getId());
        assertEquals(newUser.getHashPassword(), returnedUser.getHashPassword());
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().getUserById(adminSession, newUser.getId());
        });
        assertNotNull(thrown.getMessage());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

    @Test
    void updateUserPositive2() throws Exception {
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        bootstrap.setSession(userSession);
        assertSame(userSession.getRole(), Role.ORDINARY_USER);
        assertEquals(userSession.getUserId(), user.getId());

        bootstrap.getUserEndpoint().updateUserPassword(userSession, userPassword,
                "33333333", user);
        user.setHashPassword(TransformatorToHashMD5.getHash("33333333",
                passwordSalt,
                Integer.parseInt(passwordTimes)));
        bootstrap.getSessionEndpoint().createSession("user", "33333333");
        assertEquals(user.getHashPassword(), bootstrap.getUserEndpoint()
                .getUserById(userSession, user.getId()).getHashPassword());
        bootstrap.getUserEndpoint().updateUserPassword(userSession,
                "33333333", userPassword, user);
        user.setHashPassword(TransformatorToHashMD5.getHash(userPassword, passwordSalt,
                Integer.parseInt(passwordTimes)));
        assertEquals(user.getHashPassword(), bootstrap.getUserEndpoint()
                .getUserById(userSession, user.getId()).getHashPassword());
        bootstrap.getUserEndpoint().logOutUser(userSession);
    }

    @Test
    void updateUserNegative1() throws Exception {
        user = bootstrap.getUserEndpoint().logInUser("user", userPassword);
        userSession = bootstrap.getSessionEndpoint().createSession("user", userPassword);
        assertSame(userSession.getRole(), Role.ORDINARY_USER);
        assertEquals(userSession.getUserId(), user.getId());
        @NotNull final UserDto newUser = new UserDto();
        newUser.setLogin("test1");
        newUser.setHashPassword(TransformatorToHashMD5.getHash("12345678",
                passwordSalt,
                Integer.parseInt(passwordTimes)));
        newUser.setRole(Role.ORDINARY_USER);
        newUser.setId(UUID.randomUUID().toString());
        bootstrap.getUserEndpoint().persistUser(newUser);
        @Nullable final Throwable thrown = assertThrows(Exception.class, () -> {
            bootstrap.getUserEndpoint().updateUserPassword(userSession, "12345678",
                    "33333333", newUser);
        });
        assertNotNull(thrown.getMessage());
        admin = bootstrap.getUserEndpoint().logInUser("admin", adminPassword);
        adminSession = bootstrap.getSessionEndpoint().createSession("admin", adminPassword);
        bootstrap.getUserEndpoint().removeUserById(adminSession, newUser.getId());
        bootstrap.getUserEndpoint().logOutUser(adminSession);
    }

}