package com.iteco.linealex.sp.api.endpoint;

import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IPropertyEndpoint {

    @Nullable
    @WebMethod
    String getProperty(
            @WebParam(name = "propertyName", partName = "propertyName") @Nullable final String propertyName);

}