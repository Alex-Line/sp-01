package com.iteco.linealex.sp.api.endpoint;

import com.iteco.linealex.sp.dto.SessionDto;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    SessionDto createSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws Exception;

    @WebMethod
    void removeSession(
            @WebParam(name = "sessionId", partName = "sessionId") @NotNull final String sessionId
    ) throws Exception;

    @Nullable
    @WebMethod
    SessionDto findSession(
            @WebParam(name = "sessionId", partName = "sessionId") @NotNull final String sessionId
    ) throws Exception;

}