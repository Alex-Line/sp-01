package com.iteco.linealex.sp.api.endpoint;

import com.iteco.linealex.sp.dto.SessionDto;
import com.iteco.linealex.sp.dto.UserDto;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IUserEndpoint {

    @Nullable
    @WebMethod
    UserDto getUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String entityId
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<UserDto> getAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void persistUser(
            @WebParam(name = "user", partName = "user") @Nullable final UserDto entity
    ) throws Exception;

    @WebMethod
    void persistUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "users", partName = "users") @NotNull final Collection<UserDto> collection
    ) throws Exception;

    @WebMethod
    void removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @WebMethod
    void removeAllUsersWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception;

    @WebMethod
    void removeAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @Nullable
    @WebMethod
    UserDto logInUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws Exception;

    @WebMethod
    public void logOutUser(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws Exception;

    @WebMethod
    void createUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "user", partName = "user") @Nullable final UserDto user,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception;

    @WebMethod
    void mergeUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "user", partName = "user") @Nullable final UserDto entity
    ) throws Exception;

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "oldPassword", partName = "oldPassword") @Nullable final String oldPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception;

    @Nullable
    @WebMethod
    UserDto getUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<UserDto> getAllUsersBySelectedUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception;

    @WebMethod
    void loadBinary(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void loadFasterJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void loadFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void loadJaxbJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void loadJaxbXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void saveBinary(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void saveFasterJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void saveFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void saveJaxbJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

    @WebMethod
    void saveJaxbXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception;

}