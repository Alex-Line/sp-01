package com.iteco.linealex.sp.api.repository;

import com.iteco.linealex.sp.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public interface IProjectRepository {

    boolean contains(@NotNull final String entityId);

    @NotNull
    Collection<Project> findAll();

    @NotNull
    Collection<Project> findAll(@NotNull final String userId);

    @NotNull
    Collection<Project> findAll(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    @Nullable
    Project findOne(@NotNull final String id);

    @NotNull
    Collection<Project> findAllByName(
            @NotNull final String userId,
            @NotNull final String pattern
    );

    @NotNull
    Collection<Project> findAllByName(@NotNull final String pattern);

    @NotNull
    Collection<Project> findAllSortedByStartDate(@NotNull final String userId);

    @NotNull
    Collection<Project> findAllSortedByStartDate();

    @NotNull
    Collection<Project> findAllSortedByFinishDate(@NotNull final String userId);

    @NotNull
    Collection<Project> findAllSortedByFinishDate();

    @NotNull
    Collection<Project> findAllSortedByStatus(@NotNull final String userId);

    @NotNull
    Collection<Project> findAllSortedByStatus();

    @Nullable
    Project findOneByName(@NotNull final String entityName);

    @Nullable
    Project findOneByName(
            @NotNull final String userId,
            @NotNull final String entityName
    );

    void persist(@NotNull final Project example);

    void persist(@NotNull final Collection<Project> collection);

    void merge(@NotNull final Project example);

    void remove(@NotNull final String entityId);

    @Transactional
    void removeAll();

    void removeAll(@NotNull final String userId);

}