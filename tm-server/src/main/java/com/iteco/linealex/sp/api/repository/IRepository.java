package com.iteco.linealex.sp.api.repository;

import com.iteco.linealex.sp.exception.entity.InsertExistingEntityException;
import org.jetbrains.annotations.NotNull;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Collection;

public interface IRepository<T> {

    boolean contains(@NotNull final String entityId) throws SQLException;

    @NotNull
    Collection<T> findAll() throws SQLException;

    void persist(@NotNull final T example) throws InsertExistingEntityException, NoSuchAlgorithmException, SQLException;

    void persist(@NotNull final Collection<T> collection) throws SQLException;

    void merge(@NotNull final T example) throws InsertExistingEntityException, SQLException;

    void remove(@NotNull final String name) throws SQLException;

    void removeAll() throws SQLException;

}