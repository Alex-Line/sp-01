package com.iteco.linealex.sp.api.repository;

import com.iteco.linealex.sp.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ISessionRepository {

    boolean contains(@NotNull final String entityId);

    @NotNull
    Collection<Session> findAll();

    void remove(@NotNull final String entityId);

    void removeAll();

    void removeAll(@NotNull final String userId);

    @NotNull
    Collection<Session> findAll(@NotNull final String userId);

    @Nullable
    Session findOne(@NotNull final String entityId);

    void persist(@NotNull final Session example);

    void persist(@NotNull final Collection<Session> collection);

    void merge(@NotNull final Session example);

}