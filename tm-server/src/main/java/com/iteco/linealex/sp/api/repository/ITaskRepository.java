package com.iteco.linealex.sp.api.repository;

import com.iteco.linealex.sp.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITaskRepository {

    @NotNull
    Collection<Task> findAll(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    @Nullable
    Task findOne(@NotNull final String entityId);

    @NotNull
    Collection<Task> findAll(@NotNull final String userId);

    @Nullable
    Task findOneByName(@NotNull final String entityName);

    @Nullable
    Task findOneByName(
            @NotNull final String userId,
            @NotNull final String taskName
    );

    @Nullable
    Task findOneByName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskName
    );

    void persist(@NotNull final Task example);

    void persist(@NotNull final Collection<Task> collection);

    void merge(@NotNull final Task example);

    void remove(@NotNull final String entityId);

    void removeAll();

    void removeAll(@NotNull final String userId);

    void removeAll(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    boolean contains(@NotNull final String entityId);

    @NotNull
    Collection<Task> findAll();

    @NotNull
    Collection<Task> findAllByName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String pattern
    );

    @NotNull
    Collection<Task> findAllByName(
            @NotNull final String userId,
            @NotNull final String pattern
    );

    @NotNull
    Collection<Task> findAllByName(@NotNull final String pattern);

    @NotNull
    Collection<Task> findAllSortedByStartDate(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    @NotNull
    Collection<Task> findAllSortedByStartDate(@NotNull final String userId);

    @NotNull
    Collection<Task> findAllSortedByStartDate();

    @NotNull
    Collection<Task> findAllSortedByFinishDate(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    @NotNull
    Collection<Task> findAllSortedByFinishDate(@NotNull final String userId);

    @NotNull
    Collection<Task> findAllSortedByFinishDate();

    @NotNull
    Collection<Task> findAllSortedByStatus(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    @NotNull
    Collection<Task> findAllSortedByStatus(@NotNull final String userId);

    @NotNull
    Collection<Task> findAllSortedByStatus();

}