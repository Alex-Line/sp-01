package com.iteco.linealex.sp.api.repository;

import com.iteco.linealex.sp.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IUserRepository {

    boolean contains(@NotNull final String entityId);

    @NotNull
    Collection<User> findAll();

    @Nullable
    User findOneByName(
            @NotNull final String login,
            @NotNull final String hashPassword
    );

    void persist(@NotNull final User example);

    void persist(@NotNull final Collection<User> collection);

    @NotNull
    Collection<User> findAll(@NotNull final String userId);

    @Nullable
    User findOne(@NotNull final String entityId);

    void merge(@NotNull final User example);

    void remove(@NotNull final String entityId);

    void removeAll();

    void removeAll(@NotNull final String userId);

}