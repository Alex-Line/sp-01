package com.iteco.linealex.sp.api.service;

import org.jetbrains.annotations.Nullable;

@Deprecated
public interface IPropertyService {

    @Nullable
    String getProperty(@Nullable final String propertyName);

}