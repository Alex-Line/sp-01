package com.iteco.linealex.sp.api.service;

import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.entity.Task;
import com.iteco.linealex.sp.entity.User;
import org.jetbrains.annotations.Nullable;

@Deprecated
public interface ISelectedEntityService {

    @Nullable
    public Project getSelectedProject();

    @Nullable
    public Task getSelectedTask();

    @Nullable
    public User getSelectedUser();

    public void setSelectedProject(@Nullable final Project selectedProject);

    public void setSelectedTask(@Nullable final Task selectedTask);

    public void setSelectedUser(@Nullable final User selectedUser);

    public void clearSelection();

}