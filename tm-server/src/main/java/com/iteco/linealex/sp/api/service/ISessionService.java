package com.iteco.linealex.sp.api.service;

import com.iteco.linealex.sp.entity.Session;
import org.jetbrains.annotations.Nullable;

public interface ISessionService extends IService<Session> {

    @Nullable
    public Session getSession(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

}