package com.iteco.linealex.sp.api.service;

import com.iteco.linealex.sp.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IUserService extends IService<User> {

    @Nullable
    public User logInUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception;

    void createUser(
            @Nullable final User user,
            @Nullable final User selectedUser
    ) throws Exception;

    public void updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception;

    @Nullable
    public User getUser(
            @Nullable final String login,
            @Nullable final User selectedUser
    ) throws Exception;

    @NotNull
    public Collection<User> getAllUsers(
            @Nullable final User selectedUser
    ) throws Exception;

}