package com.iteco.linealex.sp.api.service;

import org.jetbrains.annotations.NotNull;

@Deprecated
public interface ServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    ITerminalService getTerminalService();

}