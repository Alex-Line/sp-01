package com.iteco.linealex.sp.context;

import com.iteco.linealex.sp.api.endpoint.IProjectEndpoint;
import com.iteco.linealex.sp.api.endpoint.ISessionEndpoint;
import com.iteco.linealex.sp.api.endpoint.ITaskEndpoint;
import com.iteco.linealex.sp.api.endpoint.IUserEndpoint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.xml.ws.Endpoint;

@Getter
@Setter
@Component
@NoArgsConstructor
@PropertySource("classpath:application.properties")
public final class Bootstrap {

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    public void start() {
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskService?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userService?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionService?wsdl", sessionEndpoint);
    }

}