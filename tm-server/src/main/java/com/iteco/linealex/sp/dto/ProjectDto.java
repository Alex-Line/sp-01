package com.iteco.linealex.sp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.iteco.linealex.sp.api.service.IUserService;
import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.enumerate.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDto {

    @Autowired
    static IUserService userService;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    String name;

    @Nullable
    String description;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateStart;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateFinish;

    @Nullable
    String userId;

    Status status;

    @Nullable
    public static Project toProject(@NotNull final ProjectDto projectDto) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(projectDto.getId());
        @Nullable final User user = userService.getEntityById(projectDto.getUserId());
        if (user == null) return null;
        project.setUser(user);
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setStatus(projectDto.getStatus());
        project.setDateStart(projectDto.getDateStart());
        project.setDateFinish(projectDto.getDateFinish());
        return project;
    }

}