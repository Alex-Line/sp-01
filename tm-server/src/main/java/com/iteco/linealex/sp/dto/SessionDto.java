package com.iteco.linealex.sp.dto;

import com.iteco.linealex.sp.api.service.IUserService;
import com.iteco.linealex.sp.entity.Session;
import com.iteco.linealex.sp.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class SessionDto {

    @Autowired
    static IUserService userService;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull String userId;

    @NotNull Role role;

    @NotNull Date creationDate = new Date(System.currentTimeMillis());

    @NotNull
    public static Session toSession(@NotNull final SessionDto sessionDto) throws Exception {
        @NotNull final Session session = new Session();
        session.setRole(sessionDto.getRole());
        session.setId(sessionDto.getId());
        session.setCreationDate(sessionDto.getCreationDate());
        session.setUser(userService.getEntityById(sessionDto.userId));
        return session;
    }

}