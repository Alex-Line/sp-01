package com.iteco.linealex.sp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.iteco.linealex.sp.api.service.IProjectService;
import com.iteco.linealex.sp.api.service.IUserService;
import com.iteco.linealex.sp.entity.Task;
import com.iteco.linealex.sp.enumerate.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class TaskDto {

    @Autowired
    static IProjectService projectService;

    @Autowired
    static IUserService userService;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    String name;

    @Nullable
    String description;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateStart;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateFinish;

    @NotNull
    String userId;

    @NotNull
    Status status;

    @Nullable
    private String projectId;

    @NotNull
    public static Task toTask(@NotNull final TaskDto taskDto) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(taskDto.getId());
        task.setUser(userService.getEntityById(taskDto.getUserId()));
        task.setProject(projectService.getEntityById(taskDto.getProjectId()));
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setStatus(taskDto.getStatus());
        task.setDateStart(taskDto.getDateStart());
        task.setDateFinish(taskDto.getDateFinish());
        return task;
    }

}