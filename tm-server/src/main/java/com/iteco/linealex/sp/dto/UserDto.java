package com.iteco.linealex.sp.dto;

import com.iteco.linealex.sp.api.service.IProjectService;
import com.iteco.linealex.sp.api.service.ITaskService;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UserDto {

    @Autowired
    static IProjectService projectService;

    @Autowired
    static ITaskService taskService;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String login = "unnamed";

    @Nullable
    private String hashPassword;

    @NotNull
    private Role role = Role.ORDINARY_USER;

    @NotNull
    public static User toUser(@NotNull final UserDto userDto) throws Exception {
        @NotNull final User user = new User();
        user.setId(userDto.getId());
        user.setLogin(userDto.getLogin());
        user.setHashPassword(userDto.getHashPassword());
        user.setRole(userDto.getRole());
        user.setProjects(new ArrayList<>(projectService.getAllEntities(userDto.getId())));
        user.setTasks(new ArrayList<>(taskService.getAllEntities(userDto.getId())));
        return user;
    }

}