package com.iteco.linealex.sp.endpoint;

import com.iteco.linealex.sp.api.endpoint.IPropertyEndpoint;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Deprecated
@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.IPropertyEndpoint")
public class PropertyEndpoint extends AbstractEndpoint implements IPropertyEndpoint {

    @Nullable
    @Override
    @WebMethod
    public String getProperty(
            @WebParam(name = "propertyName", partName = "propertyName") @Nullable final String propertyName) {
        //return getBootstrap().getPropertyService().getProperty(propertyName);
        return null;
    }

}