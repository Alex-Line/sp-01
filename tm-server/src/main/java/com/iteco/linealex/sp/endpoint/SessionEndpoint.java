package com.iteco.linealex.sp.endpoint;

import com.iteco.linealex.sp.api.endpoint.ISessionEndpoint;
import com.iteco.linealex.sp.api.service.ISessionService;
import com.iteco.linealex.sp.api.service.IUserService;
import com.iteco.linealex.sp.dto.SessionDto;
import com.iteco.linealex.sp.entity.Session;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.exception.user.UserIsNotExistException;
import com.iteco.linealex.sp.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;

@Component
@WebService(endpointInterface = "com.iteco.linealex.sp.api.endpoint.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Autowired
    IUserService userService;

    @Autowired
    ISessionService sessionService;

    @Override
    @WebMethod
    public SessionDto createSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws Exception {
        @NotNull final Session session = new Session();
        @Nullable final User user = userService.logInUser(login, password);
        if (user == null) throw new UserIsNotExistException();
        session.setUser(user);
        session.setRole(user.getRole());
        session.setCreationDate(new Date());
        session.setSignature(ApplicationUtils.getSignature(Session.toSessionDto(session)));
        sessionService.persist(session);
        return Session.toSessionDto(session);
    }

    @Override
    @WebMethod
    public void removeSession(
            @WebParam(name = "sessionId", partName = "sessionId") @NotNull final String sessionId
    ) throws Exception {
        sessionService.removeEntity(sessionId);
    }

    @Nullable
    @Override
    @WebMethod
    public SessionDto findSession(
            @WebParam(name = "sessionId", partName = "sessionId") @Nullable final String sessionId
    ) throws Exception {
        if (sessionId == null) return null;
        @Nullable final Session session = sessionService.getEntityById(sessionId);
        if (session == null) return null;
        return Session.toSessionDto(session);
    }

}