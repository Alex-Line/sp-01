package com.iteco.linealex.sp.endpoint;

import com.iteco.linealex.sp.api.endpoint.ITaskEndpoint;
import com.iteco.linealex.sp.api.service.ITaskService;
import com.iteco.linealex.sp.dto.SessionDto;
import com.iteco.linealex.sp.dto.TaskDto;
import com.iteco.linealex.sp.entity.Task;
import com.iteco.linealex.sp.enumerate.Role;
import com.iteco.linealex.sp.exception.user.LowAccessLevelException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Component
@WebService(endpointInterface = "com.iteco.linealex.sp.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Autowired
    ITaskService taskService;

    @Nullable
    @Override
    @WebMethod
    public TaskDto getTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        @Nullable final Task task = taskService.getEntityById(entityId);
        if (task == null) return null;
        if (session == null) return null;
        if (task.getUser().getId().equals(session.getUserId()) || session.getRole().equals(Role.ADMINISTRATOR)) {
            return Task.toTaskDto(task);
        }
        return null;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        if (session.getRole() != Role.ADMINISTRATOR) return Collections.EMPTY_LIST;
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntities()));
    }

    @Override
    @WebMethod
    public void persistTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "tasks", partName = "tasks") @NotNull final Collection<TaskDto> collection
    ) throws Exception {
        validateSession(session);
        for (@NotNull final TaskDto task : collection) {
            taskService.persist(TaskDto.toTask(task));
        }
    }

    @Override
    @WebMethod
    public void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        taskService.removeEntity(entityId);
    }

    @Override
    @WebMethod
    public void removeAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        taskService.removeAllEntities(userId);
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        taskService.removeAllEntities();
    }

    @Override
    @WebMethod
    public void persistTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "task", partName = "task") @Nullable final TaskDto entity
    ) throws Exception {
        validateSession(session);
        if (entity == null) throw new Exception("Null task");
        if (session == null) throw new Exception("Null session");
        @NotNull final Task task = TaskDto.toTask(entity);
        if (!task.getUser().getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        taskService.persist(task);
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "task", partName = "task") @Nullable final TaskDto entity
    ) throws Exception {
        validateSession(session);
        @NotNull final Task task = TaskDto.toTask(entity);
        if (!task.getUser().getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        taskService.merge(task);

    }

    @Nullable
    @Override
    @WebMethod
    public TaskDto getTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws Exception {
        validateSession(session);
        if (entityName == null || entityName.isEmpty()) return null;
        @Nullable final Task task = taskService.getEntityByName(entityName);
        if (task == null) return null;
        if (session == null) return null;
        if (!task.getUser().getId().equals(session.getUserId()) || session.getRole() != Role.ADMINISTRATOR) {
            throw new LowAccessLevelException();
        }
        return Task.toTaskDto(task);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDto getTaskByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String entityName
    ) throws Exception {
        validateSession(session);
        @Nullable final Task task = taskService.getEntityByName(userId, entityName);
        if (task == null) return null;
        if (session == null) return null;
        if (task.getUser().getId().equals(session.getUserId()) || session.getRole().equals(Role.ADMINISTRATOR)) {
            return Task.toTaskDto(task);
        }
        return null;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksByNameWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntitiesByName(userId, pattern)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntitiesByName(pattern)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksSortedByStartDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntitiesSortedByStartDate()));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksSortedByStartDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntitiesSortedByStartDate(userId)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksSortedByFinishDate(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntitiesSortedByFinishDate()));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksSortedByFinishDateWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntitiesSortedByFinishDate(userId)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksSortedByStatus(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntitiesSortedByStatus()));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksSortedByStatusWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntitiesSortedByStatus(userId)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntities(userId)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntities(userId, projectId)));
    }

    @Override
    @WebMethod
    public void removeAllTasksFromProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        taskService.removeAllTasksFromProject(userId, projectId);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDto getTaskByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName
    ) throws Exception {
        validateSession(session);
        @Nullable final Task task = taskService
                .getEntityByName(userId, projectId, taskName);
        if (task == null) return null;
        return Task.toTaskDto(task);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksByNameWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "pattern", partName = "pattern") @Nullable final String pattern
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService
                .getAllEntitiesByName(userId, projectId, pattern)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksSortedByStartDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService
                .getAllEntitiesSortedByStartDate(userId, projectId)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksSortedByFinishDateWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService
                .getAllEntitiesSortedByFinishDate(userId, projectId)));
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<TaskDto> getAllTasksSortedByStatusWithUserIdAndProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        return Task.toTasksDto(new ArrayList<>(taskService
                .getAllEntitiesSortedByStatus(userId, projectId)));
    }

}