package com.iteco.linealex.sp.endpoint;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.iteco.linealex.sp.api.endpoint.IUserEndpoint;
import com.iteco.linealex.sp.api.service.IProjectService;
import com.iteco.linealex.sp.api.service.ISessionService;
import com.iteco.linealex.sp.api.service.ITaskService;
import com.iteco.linealex.sp.api.service.IUserService;
import com.iteco.linealex.sp.dto.Domain;
import com.iteco.linealex.sp.dto.SessionDto;
import com.iteco.linealex.sp.dto.UserDto;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.enumerate.Role;
import com.iteco.linealex.sp.exception.system.ThereIsNotSuchFileException;
import com.iteco.linealex.sp.exception.user.LowAccessLevelException;
import com.iteco.linealex.sp.exception.user.UserIsNotExistException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Component
@WebService(endpointInterface = "com.iteco.linealex.sp.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Value("SAVE_PATH")
    String savePath;

    @Autowired
    IProjectService projectService;

    @Autowired
    ITaskService taskService;

    @Autowired
    IUserService userService;

    @Autowired
    ISessionService sessionService;

    @Nullable
    @Override
    @WebMethod
    public UserDto getUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String entityId
    ) throws Exception {
        validateSession(session);
        @Nullable final User user = userService.getEntityById(entityId);
        if (user == null) throw new UserIsNotExistException();
        if (!user.getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return User.toUserDto(user);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<UserDto> getAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        return User.toUsersDto(new ArrayList<>(userService.getAllEntities()));
    }

    @Override
    @WebMethod
    public void persistUser(
            @WebParam(name = "user", partName = "user") @Nullable final UserDto entity
    ) throws Exception {
        if (entity == null) throw new Exception("Null user!");
        userService.persist(UserDto.toUser(entity));
    }

    @Override
    @WebMethod
    public void persistUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "users", partName = "users") @NotNull final Collection<UserDto> collection
    ) throws Exception {
        validateSession(session);
        if (session.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        for (@Nullable final UserDto user : collection) {
            if (user == null) continue;
            persistUser(user);
        }
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        userService.removeEntity(userId);
    }

    @Override
    @WebMethod
    public void removeAllUsersWithUserId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "userId", partName = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        userService.removeEntity(userId);
    }

    @Override
    @WebMethod
    public void removeAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        userService.removeAllEntities();
    }

    @Nullable
    @Override
    @WebMethod
    public UserDto logInUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws Exception {
        return User.toUserDto(userService.logInUser(login, password));
    }

    @Override
    @WebMethod
    public void logOutUser(
            @WebParam(name = "session", partName = "session") @Nullable SessionDto session
    ) throws Exception {
        validateSession(session);
        sessionService.removeEntity(session.getId());
    }

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "user", partName = "user") @Nullable final UserDto user,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception {
        validateSession(session);
        userService.createUser(UserDto.toUser(user), UserDto.toUser(selectedUser));
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "user", partName = "user") @Nullable final UserDto entity
    ) throws Exception {
        validateSession(session);
        if (entity == null) throw new UserIsNotExistException();
        if (session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        userService.merge(UserDto.toUser(entity));
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "oldPassword", partName = "oldPassword") @Nullable final String oldPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception {
        validateSession(session);
        if (selectedUser == null) throw new UserIsNotExistException();
        if (!selectedUser.getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        userService.updateUserPassword(oldPassword, newPassword,
                UserDto.toUser(selectedUser));
    }

    @Nullable
    @Override
    @WebMethod
    public UserDto getUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception {
        validateSession(session);
        @Nullable final User user = userService.getUser(login,
                UserDto.toUser(selectedUser));
        if (user == null) throw new UserIsNotExistException();
        if (!user.getId().equals(session.getUserId()) && session.getRole() != Role.ADMINISTRATOR)
            throw new LowAccessLevelException();
        return User.toUserDto(user);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<UserDto> getAllUsersBySelectedUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDto session,
            @WebParam(name = "selectedUser", partName = "selectedUser") @Nullable final UserDto selectedUser
    ) throws Exception {
        validateSession(session);
        return User.toUsersDto(new ArrayList<>(userService.getAllUsers(UserDto.toUser(selectedUser))));
    }

    @Override
    @WebMethod
    public void loadBinary(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(savePath + "data.bin");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final FileInputStream inputStream = new FileInputStream(savedFile);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
        if (domain.getSessions() == null) sessionService.persist(Collections.EMPTY_LIST);
        else sessionService.persist(domain.getSessions());
    }

    @Override
    @WebMethod
    public void loadFasterJson(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(savePath + "data.json");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(savedFile, Domain.class);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
        if (domain.getSessions() == null) sessionService.persist(Collections.EMPTY_LIST);
        else sessionService.persist(domain.getSessions());
    }

    @Override
    @WebMethod
    public void loadFasterXml(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(savePath + "data.xml");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final XmlMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(savedFile, Domain.class);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
        if (domain.getSessions() == null) sessionService.persist(Collections.EMPTY_LIST);
        else sessionService.persist(domain.getSessions());
    }

    @Override
    @WebMethod
    public void loadJaxbJson(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(savePath + "data.json");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        System.out.println(System.getProperty("java.classpath"));
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(savedFile);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
        if (domain.getSessions() == null) sessionService.persist(Collections.EMPTY_LIST);
        else sessionService.persist(domain.getSessions());
    }

    @Override
    @WebMethod
    public void loadJaxbXml(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final File savedFile = new File(savePath + "data.xml");
        if (!savedFile.exists()) throw new ThereIsNotSuchFileException();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(savedFile);
        if (domain.getUsers() == null) userService.persist(Collections.EMPTY_LIST);
        else userService.persist(domain.getUsers());
        if (domain.getProjects() == null) projectService.persist(Collections.EMPTY_LIST);
        else projectService.persist(domain.getProjects());
        if (domain.getTasks() == null) taskService.persist(Collections.EMPTY_LIST);
        else taskService.persist(domain.getTasks());
        if (domain.getSessions() == null) sessionService.persist(Collections.EMPTY_LIST);
        else sessionService.persist(domain.getSessions());
    }

    @Override
    @WebMethod
    public void saveBinary(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService,
                taskService, sessionService);
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.bin");
        @NotNull final FileOutputStream outputStream = new FileOutputStream(saveFile);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.flush();
        objectOutputStream.close();
    }

    @Override
    @WebMethod
    public void saveFasterJson(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService,
                taskService, sessionService);
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.json");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, domain);
    }

    @Override
    @WebMethod
    public void saveFasterXml(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService,
                taskService, sessionService);
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.xml");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(saveFile, domain);
    }

    @Override
    @WebMethod
    public void saveJaxbJson(
            @Nullable final SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService,
                taskService, sessionService);
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.json");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, saveFile);
    }

    @Override
    @WebMethod
    public void saveJaxbXml(
            @Nullable SessionDto session
    ) throws Exception {
        validateSession(session);
        @NotNull final Domain domain = new Domain();
        domain.load(userService, projectService,
                taskService, sessionService);
        if (savePath == null) throw new ThereIsNotSuchFileException();
        @NotNull final File saveDir = new File(savePath);
        saveDir.mkdirs();
        @NotNull final File saveFile = new File(savePath + "data.xml");
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, saveFile);
    }

}