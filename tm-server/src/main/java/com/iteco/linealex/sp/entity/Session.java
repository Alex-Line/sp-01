package com.iteco.linealex.sp.entity;

import com.iteco.linealex.sp.dto.SessionDto;
import com.iteco.linealex.sp.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "sessions")
@NoArgsConstructor
public class Session extends AbstractEntity {

    @ManyToOne
    @NotNull User user;

    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    @NotNull Role role;

    @Basic(optional = false)
    @NotNull Date creationDate = new Date(System.currentTimeMillis());

    @Basic
    @Nullable String signature = null;

    @NotNull
    public static SessionDto toSessionDto(@NotNull final Session session) {
        @NotNull final SessionDto sessionDto = new SessionDto();
        sessionDto.setId(session.getId());
        sessionDto.setCreationDate(session.creationDate);
        sessionDto.setRole(session.role);
        sessionDto.setUserId(session.user.getId());
        return sessionDto;
    }

    @Override
    public String toString() {
        return "Session{" +
                "\n       id=" + getId() +
                ",\n      userId=" + user.getId() +
                ",\n      role=" + role +
                ",\n      creationDate=" + creationDate +
                ",\n      signature='" + signature;
    }

}