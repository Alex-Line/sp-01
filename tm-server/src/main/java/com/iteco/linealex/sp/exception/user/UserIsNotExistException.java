package com.iteco.linealex.sp.exception.user;

import com.iteco.linealex.sp.exception.TaskManagerException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UserIsNotExistException extends TaskManagerException {

    @NotNull
    @Override
    public String getMessage() {
        return "USER IS NOT EXIST. COMMAND WAS INTERRUPTED\n";
    }

}