package com.iteco.linealex.sp.repository;

import com.iteco.linealex.sp.api.repository.IRepository;
import com.iteco.linealex.sp.entity.AbstractEntity;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.SQLException;
import java.util.Collection;

@Getter
@Repository
@Transactional
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @PersistenceContext
    EntityManager entityManager;

    public abstract boolean contains(
            @NotNull final String entityId
    ) throws SQLException;

    @NotNull
    @Override
    public abstract Collection<T> findAll() throws SQLException;

    @NotNull
    public abstract Collection<T> findAll(
            @NotNull final String userId
    ) throws SQLException;

    @Nullable
    public abstract T findOne(
            @NotNull final String entityId
    ) throws SQLException;

    @Override
    public abstract void persist(
            @NotNull final T example
    ) throws SQLException;

    @Override
    public abstract void merge(
            @NotNull final T example
    ) throws SQLException;

    @Override
    public abstract void remove(
            @NotNull final String entityId
    ) throws SQLException;

    @Override
    public abstract void removeAll() throws SQLException;

    public abstract void removeAll(
            @NotNull final String userId
    ) throws SQLException;

}