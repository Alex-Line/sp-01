package com.iteco.linealex.sp.repository;

import com.iteco.linealex.sp.entity.AbstractTMEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.Collection;

public abstract class AbstractTMRepository<T extends AbstractTMEntity> extends AbstractRepository<T> {

    @Nullable
    public abstract T findOneByName(@NotNull final String entityName) throws SQLException;

    @Nullable
    public abstract T findOneByName(
            @NotNull final String userId,
            @NotNull final String entityName
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllByName(
            @NotNull final String userId,
            @NotNull final String pattern
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllByName(
            @NotNull final String pattern
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByStartDate(
            @NotNull final String userId
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByStartDate() throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByFinishDate(
            @NotNull final String userId
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByFinishDate() throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByStatus(
            @NotNull final String userId
    ) throws SQLException;

    @NotNull
    public abstract Collection<T> findAllSortedByStatus() throws SQLException;

}