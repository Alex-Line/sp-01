package com.iteco.linealex.sp.repository;

import com.iteco.linealex.sp.api.repository.ITaskRepository;
import com.iteco.linealex.sp.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public final class TaskRepository extends AbstractTMRepository<Task> implements ITaskRepository {

    @NotNull
    public Collection<Task> findAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId AND t.project.id= :projectId", Task.class)
                .setParameter("userId", userId).setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String entityId) {
        return entityManager.find(Task.class, entityId);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId", Task.class).setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String entityName) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE t.name= :entityName", Task.class)
                .setParameter("entityName", entityName).getSingleResult();
    }

    @Nullable
    @Override
    public Task findOneByName(
            @NotNull final String userId,
            @NotNull final String taskName
    ) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId AND t.name= :taskName", Task.class)
                .setParameter("taskName", taskName).setParameter("userId", userId)
                .getSingleResult();
    }

    @Nullable
    public Task findOneByName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskName
    ) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId t.project.id= :projectId AND t.name= :taskName", Task.class)
                .setParameter("taskName", taskName).setParameter("projectId", projectId)
                .setParameter("userId", userId).getSingleResult();
    }

    @Override
    public void persist(@NotNull final Task example) {
        entityManager.persist(example);
    }

    @Override
    public void persist(@NotNull final Collection<Task> collection) {
        for (Task example : collection) {
            if (example == null) continue;
            entityManager.persist(example);
        }
    }

    @Override
    public void merge(@NotNull final Task example) {
        entityManager.merge(example);
    }

    @Override
    public void remove(@NotNull final String entityId) {
        entityManager.createQuery("DELETE FROM Task t WHERE t.id= :entityId")
                .setParameter("entityId", entityId).executeUpdate();
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Task t").executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM Task t WHERE t.user.id= :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    public void removeAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        entityManager.createQuery("DELETE FROM Task t " +
                "WHERE t.user.id= :userId AND t.project.id= :projectId")
                .setParameter("userId", userId).setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public boolean contains(@NotNull final String entityId) {
        @Nullable final Task task = findOne(entityId);
        if (task != null) return true;
        return false;
    }

    @NotNull
    @Override
    public Collection<Task> findAll() {
        return entityManager.createQuery("SELECT t FROM Task t", Task.class).getResultList();
    }

    @NotNull
    public Collection<Task> findAllByName(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String pattern
    ) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId AND t.project.id= :projectId " +
                "AND (t.name LIKE CONCAT('%', :pattern, '%') " +
                "OR t.description LIKE CONCAT('%', :pattern, '%')", Task.class)
                .setParameter("userId", userId).setParameter("projectId", projectId)
                .setParameter("pattern", pattern).getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> findAllByName(
            @NotNull final String userId,
            @NotNull final String pattern
    ) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId " +
                "AND (t.name LIKE CONCAT('%', :pattern, '%') " +
                "OR t.description LIKE CONCAT('%', :pattern, '%'))", Task.class)
                .setParameter("userId", userId).setParameter("pattern", pattern)
                .getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> findAllByName(@NotNull final String pattern) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE (t.name LIKE CONCAT('%', :pattern, '%') " +
                "OR t.description LIKE CONCAT('%', :pattern, '%'))", Task.class)
                .setParameter("pattern", pattern).getResultList();
    }

    @NotNull
    public Collection<Task> findAllSortedByStartDate(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId AND t.project.id= :projectId " +
                "ORDER BY t.dateStart", Task.class)
                .setParameter("userId", userId).setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStartDate(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId ORDER BY t.dateStart", Task.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStartDate() {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "ORDER BY t.dateStart", Task.class)
                .getResultList();
    }

    @NotNull
    public Collection<Task> findAllSortedByFinishDate(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId AND t.project.id= :projectId " +
                "ORDER BY t.dateFinish", Task.class)
                .setParameter("userId", userId).setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByFinishDate(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId ORDER BY t.dateFinish", Task.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByFinishDate() {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "ORDER BY t.dateFinish", Task.class).getResultList();
    }

    @NotNull
    public Collection<Task> findAllSortedByStatus(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId AND t.project.id= :projectId " +
                "ORDER BY t.status", Task.class)
                .setParameter("userId", userId).setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStatus(
            @NotNull final String userId
    ) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE t.user.id= :userId ORDER BY t.status", Task.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> findAllSortedByStatus() {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "ORDER BY t.status", Task.class).getResultList();
    }

}