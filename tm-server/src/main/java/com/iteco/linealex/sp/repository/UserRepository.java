package com.iteco.linealex.sp.repository;

import com.iteco.linealex.sp.api.repository.IUserRepository;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.enumerate.Role;
import com.iteco.linealex.sp.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Collection;

@Repository
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Value("${ADMIN}")
    private String adminLogin;

    @Value("${ADMIN_PASSWORD}")
    private String adminPassword;

    @Value("${USER}")
    private String userLogin;

    @Value("${USER_PASSWORD}")
    private String userPassword;

    @Value("${PASSWORD_SALT}")
    private String passwordSalt;

    @Value("${PASSWORD_TIMES}")
    private String passwordTimes;

    @Value("${SESSION_LIFE_TIME}")
    private String sessionLifeTime;

    public UserRepository() {
        init();
    }

    public boolean contains(@NotNull final String entityId) {
        @NotNull final User user = findOne(entityId);
        if (user != null) return true;
        return false;
    }

    @NotNull
    @Override
    public Collection<User> findAll() {
        return entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Nullable
    public User findOneByName(
            @NotNull final String login,
            @NotNull final String hashPassword
    ) {
        return entityManager.createQuery("SELECT u FROM User u " +
                "WHERE u.login= :login and u.hashPassword= :hashPassword", User.class)
                .setParameter("login", login).setParameter("hashPassword", hashPassword)
                .getSingleResult();
    }

    @Override
    public void persist(@NotNull final User example) {
        entityManager.persist(example);
    }

    @Override
    public void persist(@NotNull final Collection<User> collection) {
        for (@Nullable final User user : collection) {
            if (user == null) continue;
            entityManager.persist(user);
        }
    }

    @NotNull
    @Override
    public Collection<User> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT u FROM User u " +
                "WHERE u.id= :userId", User.class).getResultList();
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String entityId) {
        return entityManager.find(User.class, entityId);

    }

    @Override
    public void merge(@NotNull final User example) {
        entityManager.merge(example);
    }

    @Override
    public void remove(@NotNull final String entityId) {
        entityManager.createQuery("DELETE FROM User u WHERE u.id= :entityId")
                .setParameter("entityId", entityId).executeUpdate();
    }

    @Override
    @Transactional
    public void removeAll() {
        entityManager.createQuery("DELETE FROM User u").executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM User u WHERE u.id= :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    private void init() {
        try {
            @NotNull final User admin = new User();
            admin.setLogin(adminLogin);
            admin.setHashPassword(TransformatorToHashMD5.getHash(
                    adminPassword, passwordSalt, Integer.parseInt(passwordTimes)));
            admin.setRole(Role.ADMINISTRATOR);
            persist(admin);
            @NotNull final User user = new User();
            user.setLogin(userLogin);
            user.setHashPassword(TransformatorToHashMD5.getHash(
                    userPassword, passwordSalt, Integer.parseInt(passwordTimes)));
            persist(user);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}