package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.service.IService;
import com.iteco.linealex.sp.context.Bootstrap;
import com.iteco.linealex.sp.entity.AbstractEntity;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Getter
@Setter
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @Autowired
    Bootstrap bootstrap;

    @Nullable
    @Override
    public abstract T getEntityById(
            @Nullable final String entityId
    ) throws Exception;

    @NotNull
    @Override
    public abstract Collection<T> getAllEntities() throws Exception;

    @Override
    public abstract void persist(
            @Nullable final T entity
    ) throws Exception;

    @Override
    public abstract void removeEntity(
            @Nullable final String entityId
    ) throws Exception;

    public abstract void removeAllEntities(
            @Nullable final String userId
    ) throws Exception;

    public abstract void removeAllEntities() throws Exception;

    public void setBootstrap(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

}