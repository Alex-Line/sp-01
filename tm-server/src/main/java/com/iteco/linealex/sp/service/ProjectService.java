package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.repository.IProjectRepository;
import com.iteco.linealex.sp.api.service.IProjectService;
import com.iteco.linealex.sp.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;

@Transactional
@Service("projectService")
public final class ProjectService extends AbstractTMService<Project> implements IProjectService {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    private IProjectRepository projectRepository;

    @Nullable
    @Override
    public Project getEntityById(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return null;
        @NotNull final Project project = projectRepository.findOne(entityId);
        entityManager.close();
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities() {
        @NotNull final Collection<Project> collection = projectRepository.findAll();
        entityManager.close();
        return collection;
    }

    @Override
    @Transactional
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        if (project.getName() == null || project.getName().isEmpty()) return;
        if (project.getDescription() == null || project.getDescription().isEmpty()) return;
        projectRepository.persist(project);
        entityManager.close();
    }

    @Override
    public void persist(@NotNull Collection<Project> collection) {
        if (collection.isEmpty()) return;
        for (@Nullable final Project project : collection) {
            if (project == null) continue;
            persist(project);
        }
    }

    @Override
    @Transactional
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return;
        projectRepository.remove(entityId);
        entityManager.close();
    }

    @Override
    @Transactional
    public void removeAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAll(userId);
        entityManager.close();
    }

    @Override
    @Transactional
    public void removeAllEntities() {
        projectRepository.removeAll();
        entityManager.close();
    }

    @Override
    @Transactional
    public void merge(@Nullable final Project entity) {
        if (entity == null) return;
        if (entity.getName() == null || entity.getName().isEmpty()) return;
        projectRepository.merge(entity);
        entityManager.close();
    }

    @Nullable
    @Override
    public Project getEntityByName(@Nullable final String entityName) {
        if (entityName == null || entityName.isEmpty()) return null;
        @Nullable final Project project = projectRepository.findOneByName(entityName);
        entityManager.close();
        return project;
    }

    @Nullable
    @Override
    public Project getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (entityName == null || entityName.isEmpty()) return null;
        @Nullable final Project project = projectRepository.findOneByName(userId, entityName);
        entityManager.close();
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAllByName(userId, pattern);
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesByName(@Nullable final String pattern) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAllByName(pattern);
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStartDate() {
        @NotNull final Collection<Project> collection = projectRepository.findAllSortedByStartDate();
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAllSortedByStartDate(userId);
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByFinishDate() {
        @NotNull final Collection<Project> collection = projectRepository.findAllSortedByFinishDate();
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAllSortedByFinishDate(userId);
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStatus() {
        @NotNull final Collection<Project> collection = projectRepository.findAllSortedByStatus();
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAllSortedByStatus(userId);
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAll(userId);
        entityManager.close();
        return collection;
    }

}