package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.service.ISelectedEntityService;
import com.iteco.linealex.sp.entity.Project;
import com.iteco.linealex.sp.entity.Task;
import com.iteco.linealex.sp.entity.User;
import org.jetbrains.annotations.Nullable;

@Deprecated
public final class SelectedEntityService implements ISelectedEntityService {

    @Nullable
    private Project selectedProject;

    @Nullable
    private Task selectedTask;

    @Nullable
    private User selectedUser;

    @Nullable
    @Override
    public Project getSelectedProject() {
        return selectedProject;
    }

    @Nullable
    @Override
    public Task getSelectedTask() {
        return selectedTask;
    }

    @Nullable
    @Override
    public User getSelectedUser() {
        return selectedUser;
    }

    @Override
    public void setSelectedProject(@Nullable final Project selectedProject) {
        this.selectedProject = selectedProject;
    }

    @Override
    public void setSelectedTask(@Nullable final Task selectedTask) {
        this.selectedTask = selectedTask;
    }

    @Override
    public void setSelectedUser(@Nullable final User selectedUser) {
        this.selectedUser = selectedUser;
    }

    @Override
    public void clearSelection() {
        selectedProject = null;
        selectedTask = null;
    }

}