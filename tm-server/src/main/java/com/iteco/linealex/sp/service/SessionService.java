package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.repository.ISessionRepository;
import com.iteco.linealex.sp.api.service.ISessionService;
import com.iteco.linealex.sp.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Transactional
@Service("sessionService")
public class SessionService extends AbstractService<Session> implements ISessionService {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    private ISessionRepository sessionRepository;

    @Nullable
    @Override
    public Session getEntityById(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return null;
        @Nullable final Session session = sessionRepository.findOne(entityId);
        entityManager.close();
        return session;
    }

    @NotNull
    @Override
    public Collection<Session> getAllEntities() {
        @Nullable final Collection<Session> sessions = sessionRepository.findAll();
        entityManager.close();
        return sessions;
    }

    @Override
    @Transactional
    public void persist(@Nullable final Session entity) {
        if (entity == null) return;
        if (entity.getSignature() == null || entity.getSignature().isEmpty()) return;
        sessionRepository.persist(entity);
        entityManager.close();
    }

    @Override
    @Transactional
    public void persist(@NotNull final Collection<Session> collection) {
        if (collection.isEmpty()) return;
        sessionRepository.persist(collection);
        entityManager.close();
    }

    @Override
    @Transactional
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return;
        sessionRepository.remove(entityId);
        entityManager.close();
    }

    @Override
    @Transactional
    public void removeAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        sessionRepository.removeAll(userId);
        entityManager.close();
    }

    @Override
    @Transactional
    public void removeAllEntities() {
        sessionRepository.removeAll();
        entityManager.close();
    }

    @Override
    @Transactional
    public void merge(@Nullable final Session entity) {
        if (entity == null) return;
        if (entity.getSignature() == null || entity.getSignature().isEmpty()) return;
        sessionRepository.merge(entity);
        entityManager.close();
    }

    @Nullable
    @Override
    public Session getSession(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @Nullable final Session session = sessionRepository.findOne(id);
        entityManager.close();
        return session;
    }

}