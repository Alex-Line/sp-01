package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.repository.ITaskRepository;
import com.iteco.linealex.sp.api.service.ITaskService;
import com.iteco.linealex.sp.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;

@Transactional
@Service("taskService")
public final class TaskService extends AbstractTMService<Task> implements ITaskService {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    public Collection<Task> getAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @Nullable final Collection<Task> tasks = taskRepository.findAll(userId);
        entityManager.close();
        return tasks;
    }

    @NotNull
    public Collection<Task> getAllEntities(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return getAllEntities(userId);
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @Nullable final Collection<Task> tasks = taskRepository.findAll(userId, projectId);
        entityManager.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task getEntityById(@Nullable final String entityId) {
        if (entityId == null) return null;
        @Nullable final Task task = taskRepository.findOne(entityId);
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntities() throws SQLException {
        @Nullable final Collection<Task> tasks = taskRepository.findAll();
        entityManager.close();
        return tasks;
    }

    @Override
    @Transactional
    public void persist(@Nullable final Task entity) {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return;
        taskRepository.persist(entity);
        entityManager.close();
    }

    @Override
    public void persist(@NotNull Collection<Task> collection) {
        if (collection.isEmpty()) return;
        for (@Nullable final Task task : collection) {
            if (task == null) continue;
            persist(task);
        }
    }

    @Override
    @Transactional
    public void merge(@Nullable final Task entity) {
        if (entity == null || entity.getName() == null || entity.getName().isEmpty()) return;
        taskRepository.merge(entity);
        entityManager.close();
    }

    @Nullable
    @Override
    public Task getEntityByName(@Nullable final String entityName) {
        if (entityName == null || entityName.isEmpty()) return null;
        @Nullable final Task task = taskRepository.findOneByName(entityName);
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (entityName == null || entityName.isEmpty()) return null;
        @Nullable final Task task = taskRepository.findOneByName(userId, entityName);
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @Nullable final Collection<Task> tasks = taskRepository.findAllByName(userId, pattern);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(@Nullable final String pattern) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @Nullable final Collection<Task> tasks = taskRepository.findAllByName(pattern);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate() throws Exception {
        @Nullable final Collection<Task> tasks = taskRepository.findAllSortedByStartDate();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @Nullable final Collection<Task> tasks = taskRepository.findAllSortedByStartDate(userId);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate() {
        @Nullable final Collection<Task> tasks = taskRepository.findAllSortedByFinishDate();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @Nullable final Collection<Task> tasks = taskRepository.findAllSortedByFinishDate(userId);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus() {
        @Nullable final Collection<Task> tasks = taskRepository.findAllSortedByStatus();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @Nullable final Collection<Task> tasks = taskRepository.findAllSortedByStatus(userId);
        entityManager.close();
        return tasks;
    }

    @Override
    @Transactional
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return;
        taskRepository.remove(entityId);
        entityManager.close();
    }

    @Override
    @Transactional
    public void removeAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAll(userId);
        entityManager.close();
    }

    @Override
    @Transactional
    public void removeAllEntities() {
        taskRepository.removeAll();
        entityManager.close();
    }

    @Transactional
    public void removeAllTasksFromProject(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAll(userId, projectId);
        entityManager.close();
    }

    @Nullable
    @Override
    public Task getEntityByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        @Nullable final Task task = taskRepository.findOneByName(userId, projectId, taskName);
        entityManager.close();
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String pattern
    ) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @Nullable final Collection<Task> tasks = taskRepository.findAllByName(userId, projectId, pattern);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStartDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @Nullable final Collection<Task> tasks = taskRepository.findAllSortedByStartDate(userId, projectId);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByFinishDate(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        @Nullable final Collection<Task> tasks = taskRepository.findAllSortedByFinishDate(userId, projectId);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> getAllEntitiesSortedByStatus(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (projectId == null || projectId.isEmpty()) return Collections.EMPTY_LIST;
        @Nullable final Collection<Task> tasks = taskRepository.findAllSortedByStatus(userId, projectId);
        entityManager.close();
        return tasks;
    }

}