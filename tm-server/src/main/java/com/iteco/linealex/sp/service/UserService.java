package com.iteco.linealex.sp.service;

import com.iteco.linealex.sp.api.repository.IUserRepository;
import com.iteco.linealex.sp.api.service.IUserService;
import com.iteco.linealex.sp.entity.User;
import com.iteco.linealex.sp.enumerate.Role;
import com.iteco.linealex.sp.exception.user.*;
import com.iteco.linealex.sp.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;

@Transactional
@Service("userService")
public final class UserService extends AbstractService<User> implements IUserService {

    @Value("${PASSWORD_SALT}")
    private String passwordSalt = "tzEGMt5k";

    @Value("${PASSWORD_TIMES}")
    private String passwordTimes = "5";

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    private IUserRepository userRepository;

    @Nullable
    @Override
    public User getEntityById(@Nullable final String entityId) {
        if (entityId == null) return null;
        @Nullable final User user = userRepository.findOne(entityId);
        entityManager.close();
        return user;
    }

    @NotNull
    @Override
    public Collection<User> getAllEntities() {
        @Nullable final Collection<User> users = userRepository.findAll();
        entityManager.close();
        return users;
    }

    @Nullable
    @Override
    public User logInUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        if (password == null || password.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashPassword = TransformatorToHashMD5.getHash(password,
                passwordSalt, Integer.parseInt(passwordTimes));
        @Nullable final User user = userRepository.findOneByName(login, hashPassword);
        entityManager.close();
        return user;
    }

    @Override
    @Transactional
    public void createUser(
            @Nullable final User user,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) return;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (user == null) return;
        userRepository.persist(user);
        entityManager.close();
    }

    @Override
    @Transactional
    public void persist(@Nullable final User entity) {
        if (entity == null) return;
        if (entity.getLogin().isEmpty()) return;
        if (entity.getHashPassword() == null || entity.getHashPassword().isEmpty()) return;
        userRepository.persist(entity);
        entityManager.close();
    }

    @Override
    public void persist(@NotNull Collection<User> collection) throws Exception {
        if (collection.isEmpty()) return;
        for (@Nullable final User user : collection) {
            if (user == null) continue;
            persist(user);
        }
    }

    @Override
    @Transactional
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null) return;
        userRepository.remove(entityId);
        entityManager.close();
    }

    @Override
    @Transactional
    public void removeAllEntities(@Nullable final String userId) {
        userRepository.removeAll(userId);
        entityManager.close();
    }

    @Override
    @Transactional
    public void removeAllEntities() {
        userRepository.removeAll();
        entityManager.close();
    }

    @Override
    @Transactional
    public void merge(@Nullable final User entity) {
        if (entity == null) return;
        userRepository.merge(entity);
        entityManager.close();
    }

    @Transactional
    public void updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashOldPassword = TransformatorToHashMD5.getHash(oldPassword,
                passwordSalt, Integer.parseInt(passwordTimes));
        if (selectedUser.getHashPassword() == null
                && !selectedUser.getHashPassword().equals(hashOldPassword)) throw new WrongPasswordException();
        if (newPassword == null || newPassword.isEmpty()) throw new WrongPasswordException();
        if (newPassword.length() < 8) throw new ShortPasswordException();
        @NotNull final String hashNewPassword = TransformatorToHashMD5.getHash(newPassword,
                passwordSalt, Integer.parseInt(passwordTimes));
        selectedUser.setHashPassword(hashNewPassword);
        userRepository.merge(selectedUser);
        entityManager.close();
    }

    @Nullable
    public User getUser(
            @Nullable final String login,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        @Nullable final User user = userRepository.findOneByName(login, selectedUser.getHashPassword());
        entityManager.close();
        return user;
    }

    @NotNull
    public Collection<User> getAllUsers(
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) return Collections.EMPTY_LIST;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        @NotNull final Collection<User> user = userRepository.findAll(selectedUser.getId());
        entityManager.close();
        return user;
    }

}