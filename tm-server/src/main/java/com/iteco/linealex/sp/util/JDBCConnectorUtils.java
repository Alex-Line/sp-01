package com.iteco.linealex.sp.util;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.DriverManager;

public class JDBCConnectorUtils {

    @NotNull
    public static Connection getConnection(
            @NotNull final String url,
            @NotNull final String login,
            @NotNull final String password
    ) throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
        @NotNull final Connection connection = DriverManager.getConnection(url, login, password);
        //connection.setAutoCommit(false);
        return connection;
    }

}