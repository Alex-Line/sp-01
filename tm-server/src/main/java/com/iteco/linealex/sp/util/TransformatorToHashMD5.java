package com.iteco.linealex.sp.util;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class TransformatorToHashMD5 {

    @NotNull
    public static String getHash(
            @NotNull final String convertingString,
            @NotNull final String salt,
            int times
    ) throws NoSuchAlgorithmException {
        @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
        @NotNull StringBuffer hexString = new StringBuffer(convertingString);
        for (int i = 0; i < times; i++) {
            hexString.append(salt);
            md.update(hexString.toString().getBytes());
            hexString = new StringBuffer();
            for (byte aByteData : md.digest()) {
                @NotNull final String hex = Integer.toHexString(0xff & aByteData);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
        }
        md.update(convertingString.getBytes());
        for (byte aByteData : md.digest()) {
            @NotNull final String hex = Integer.toHexString(0xff & aByteData);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

}